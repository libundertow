#!/bin/sh

echo "Bootstrapping libundertow build environment..."
set -x
aclocal
autoconf
libtoolize --copy
automake --add-missing --copy --gnu
