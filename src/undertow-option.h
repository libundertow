/* libundertow - undertow-option.h
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-option.h */

#ifndef LIBUNDERTOW_OPTION_H
#define LIBUNDERTOW_OPTION_H

#ifdef __cplusplus
extern "C" {
#endif

void lut_option_set_database_path (const char *path);
char *lut_option_get_database_path (void);
void lut_option_set_download_directory (const char *path);
char *lut_option_get_download_directory (void);
void lut_option_set_default_refresh_period (int minutes);
int lut_option_get_default_refresh_period (void);

#ifdef __cplusplus
}
#endif

#endif
