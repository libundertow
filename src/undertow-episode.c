/* libundertow - undertow-episode.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-episode.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include <sqlite3.h>

#include "undertow-db-private.h"
#include "undertow-episode.h"
#include "undertow-error.h"
#include "undertow-logging.h"

/** Allocate a new #lut_episode instance
 *
 * @return A new #lut_episode instance
 */
lut_episode *
lut_episode_new (void)
{
    lut_episode *episode;

    episode = malloc (sizeof (lut_episode));
    memset (episode, 0, sizeof (lut_episode));
    return episode;
}

/** Allocate a new #lut_episode instance with the specified values
 *
 * @param channel
 * @param content_url
 * @param published_on
 * @param content_length
 * @param content_type
 * @param title
 * @param description
 * @return A new #lut_episode instance
 */
lut_episode *
lut_episode_new_with_values (const char *          channel,
                                  const char *          content_url,
                                  time_t                published_on,
                                  size_t                content_length,
                                  const char *          content_type,
                                  const char *          title,
                                  const char *          description,
                                  const char *          file_path)
{
    lut_episode *episode;

    episode = malloc (sizeof (lut_episode));
    memset (episode, 0, sizeof (lut_episode));
    if (channel)
        episode->channel = strdup (channel);
    if (content_url)
        episode->content_url = strdup (content_url);
    episode->published_on = published_on;
    episode->content_length = content_length;
    if (content_type)
        episode->content_type = strdup (content_type);
    if (title)
        episode->title = strdup (title);
    if (description)
        episode->description = strdup (description);
    return episode;
}

/** Copy a #lut_episode instance
 *
 * @param episode The #lut_episode instance to copy
 * @return A new #lut_episode instance
 */
lut_episode *
lut_episode_copy (lut_episode *episode)
{
    lut_episode *copy;

    assert (episode != NULL);
    copy = malloc (sizeof (lut_episode));
    memset (copy, 0, sizeof (lut_episode));
    copy->channel = strdup (episode->channel);
    copy->content_url = strdup (episode->content_url);
    copy->published_on = episode->published_on;
    copy->content_length = episode->content_length;
    copy->content_type = strdup (episode->content_type);
    copy->title = strdup (episode->title);
    copy->description = strdup (episode->description);
    copy->file_path = strdup (episode->file_path);
    return copy;
}

/** Free a #lut_episode instance
 *
 * @param episode The #lut_episode instance to free
 */
void 
lut_episode_free (lut_episode *episode)
{
    assert (episode != NULL);

    if (episode->channel)
        free (episode->channel);
    if (episode->content_url)
        free (episode->content_url);
    if (episode->content_type)
        free (episode->content_type);
    if (episode->title)
        free (episode->title);
    if (episode->description)
        free (episode->description);
    if (episode->file_path)
        free (episode->file_path);
    free (episode);
}

/** Free a list of episodes
 *
 * @param episodes Array of episodes to free
 * @param n_episodes Size of the array
 */
void
lut_episode_free_list (lut_episode *episodes[], size_t n_episodes)
{
    int i;

    assert (episodes != NULL);

    for (i = 0; i < n_episodes; i++) {
        if (episodes[i] != NULL)
            lut_episode_free (episodes[i]);
    }
    free (episodes);
}

/** Retrieve an episode from the database
 *
 * @param channel The URL of the channel which contains the episode
 * @param content_url The URL of the content to retrieve
 * @param episode Upon success, episode is set to the location of a newly allocated #lut_episode
 * @return LUT_OK on success, otherwise a #lut_errno corresponding to the error.
 */
lut_errno
lut_get_episode (const char *channel, const char *content_url, lut_episode **episode)
{
    sqlite3 *db;
    int retval;
    sqlite3_stmt *statement = NULL;
    char *sql;
    const char *tail;

    assert (channel != NULL);
    assert (content_url != NULL);

    db = lut_db_begin_transaction ();
    sql = sqlite3_mprintf ("SELECT content_size,content_type,title,description,published_on,file_path"
                           " FROM episodes"
                           " WHERE channel=%Q AND content_url=%Q;",
                           channel,
                           content_url);
    retval = sqlite3_prepare (db, sql, strlen (sql), &statement, &tail);
    sqlite3_free (sql);
    if (retval != SQLITE_OK) {
        lut_log_error ("lut_db_get_episode: %s", sqlite3_errmsg (db));
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_FAILURE;
    }

    retval = sqlite3_step (statement);
    if (retval == SQLITE_ROW) {
        const unsigned char *content_type, *title, *description, *file_path;
        size_t content_length;
        time_t published_on;
        lut_episode *episode_ptr;

        content_length = (size_t) sqlite3_column_int (statement, 0);
        content_type = sqlite3_column_text (statement, 1);
        title = sqlite3_column_text (statement, 2);
        description = sqlite3_column_text (statement, 3);
        published_on = sqlite3_column_int (statement, 4);
        file_path = sqlite3_column_text (statement, 5);

        episode_ptr = malloc (sizeof (lut_episode));
        memset (episode_ptr, 0, sizeof (lut_episode));
        episode_ptr->channel = strdup (channel);
        episode_ptr->content_url = strdup (content_url);
        episode_ptr->content_length = content_length;
        if (content_type)
            episode_ptr->content_type = strdup ((char *)content_type);
        if (title)
            episode_ptr->title = strdup ((char *)title);
        if (description)
            episode_ptr->description = strdup ((char *)description);
        episode_ptr->published_on = published_on;
        if (file_path)
            episode_ptr->file_path = strdup ((char *)file_path);
        *episode = episode_ptr;
    }
    else
        *episode = NULL;

    sqlite3_finalize (statement);
    lut_db_commit_transaction ();
    return LUT_OK;
}

/** Test whether an episode exists in the database
 *
 * @param channel The URL of the channel which contains the episode
 * @param content_url The URL of the content to retrieve
 * @return LUT_OK if the episode exists, LUT_ERROR_DB_NO_SUCH_ITEM if there
 * is no such episode, otherwise a #lut_errno corresponding to the error.
 */
lut_errno
lut_episode_exists (const char *channel, const char *content_url)
{
    sqlite3 *db;
    int retval, exists;
    sqlite3_stmt *statement = NULL;
    char *sql;
    const char *tail;

    assert (channel != NULL);
    assert (content_url != NULL);

    db = lut_db_begin_transaction ();
    sql = sqlite3_mprintf ("SELECT published_on"
                           " FROM episodes"
                           " WHERE channel=%Q and content_url=%Q;",
                           channel,
                           content_url);
    retval = sqlite3_prepare (db, sql, strlen (sql), &statement, &tail);
    sqlite3_free (sql);
    if (retval == SQLITE_OK) {
        if (sqlite3_step (statement) == SQLITE_ROW)
            exists = LUT_OK;
        else
            exists = LUT_ERROR_DB_NO_SUCH_ITEM;
    }
    else  {
        exists = 0;
        lut_log_error ("lut_db_get_episode: %s", sqlite3_errmsg (db));
    }
    sqlite3_finalize (statement);
    lut_db_commit_transaction ();
    return exists;
}

/** Perform an operation on each episode in a channel
 *
 * @param channel The URL of the channel to iterate over
 * @param f The function to execute
 * @param data A pointer to data which is passed unmodified to f
 * @return LUT_OK on success, otherwise a #lut_errno corresponding to the error.
 */
lut_errno
lut_foreach_episode (const char *channel, lut_foreach_episode_funct f, void *data)
{
    sqlite3 *db;
    sqlite3_stmt *statement = NULL;
    const char *tail;
    char *sql;
    lut_episode **episodes = NULL;
    int n_episodes = 0, i, retval;

    assert (f != NULL);

    db = lut_db_begin_transaction ();
    sql = sqlite3_mprintf ("SELECT content_url,content_size,content_type,title,description,published_on,file_path"
                           " FROM episodes"
                           " WHERE channel=%Q", channel);
    retval = sqlite3_prepare (db, sql, strlen (sql), &statement, &tail);
    sqlite3_free (sql);
    if (retval != SQLITE_OK) {
        lut_log_error ("lut_db_foreach_channel: %s", sqlite3_errmsg (db));
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_FAILURE;
    }

    retval = sqlite3_step (statement);
    while (retval == SQLITE_ROW) {
        const unsigned char *content_url, *content_type, *title, *description, *file_path;
        size_t content_length;
        time_t published_on;
        lut_episode *episode;

        content_url = sqlite3_column_text (statement, 0);
        content_length = (size_t) sqlite3_column_int (statement, 1);
        content_type = sqlite3_column_text (statement, 2);
        title = sqlite3_column_text (statement, 3);
        description = sqlite3_column_text (statement, 4);
        published_on = sqlite3_column_int (statement, 5);
        file_path = sqlite3_column_text (statement, 6);

        episode = malloc (sizeof (lut_episode));
        memset (episode, 0, sizeof (lut_episode));
        episode->channel = strdup (channel);
        episode->content_url = strdup ((char *)content_url);
        episode->content_length = content_length;
        if (content_type)
            episode->content_type = strdup ((char *)content_type);
        if (title)
            episode->title = strdup ((char *)title);
        if (description)
            episode->description = strdup ((char *)description);
        episode->published_on = published_on;
        if (file_path)
            episode->file_path = strdup ((char *)file_path);

        episodes = realloc (episodes, sizeof (lut_episode *) * (n_episodes + 1));
        episodes[n_episodes++] = episode;

        retval = sqlite3_step (statement);
    }
    sqlite3_finalize (statement);
    lut_db_commit_transaction ();

    for (i = 0; i < n_episodes; i++)
        f (episodes[i], data);
    free (episodes);
    return LUT_OK;
}
