/* libundertow - undertow-error.h
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-error.h */

#ifndef LIBUNDERTOW_ERROR_H
#define LIBUNDERTOW_ERROR_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup ErrorHandling
 * @{
 */

/** Undertow error
 */
typedef enum {
    LUT_OK = 0,                        /**< No error. */
    LUT_ERROR_FILE_NOT_FOUND,          /**< File not found. */
    LUT_ERROR_NET_FAILURE,             /**< Generic network operation failure. */
    LUT_ERROR_NET_HOST_LOOKUP_FAILED,  /**< DNS hostname lookup failed. */
    LUT_ERROR_NET_NOT_FOUND,           /**< Remote item was not found. */
    LUT_ERROR_NET_FORBIDDEN,           /**< Access denied to remote item. */
    LUT_ERROR_NET_UNAUTHORIZED,        /**< Need authorization to access remote item */
    LUT_ERROR_DB_FAILURE,              /**< Generic database failure. */
    LUT_ERROR_DB_ITEM_EXISTS,          /**< Channel already exists in the database. */
    LUT_ERROR_DB_NO_SUCH_ITEM,         /**< Channel doesn't exist in the database. */
    LUT_ERROR_FEED_INVALID,            /**< The feed data is invalid. */
    LUT_ERROR_DEFERRED_WAS_CANCELLED,  /**< Operation was cancelled. */
    LUT_ERROR_DEFERRED_NO_SUCH_ITEM,   /**< Deferred item is no longer running. */
    LUT_ERROR_DISPATCHER_IS_RUNNING,   /**< Dispatcher is already running. */
    LUT_ERROR_DISPATCHER_NOT_RUNNING,  /**< Dispatcher is not running. */
    LUT_ERROR_MAINLOOP_IS_RUNNING,     /**< Main loop is already running. */
    LUT_ERROR_MAINLOOP_NOT_RUNNING,    /**< Main loop is not running. */
    LUT_LAST_ERROR                     /**< */
} lut_errno;

/** @} */

const char *lut_strerror (lut_errno error);

#ifdef __cplusplus
}
#endif

#endif
