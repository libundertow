/* libundertow - undertow-logging.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-logging.c */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <pthread.h>

#include "undertow-logging.h"

static void
default_logging_funct (lut_log_severity s, const char *message)
{
    switch (s) {
        case LUT_LOG_ERROR:
            fprintf (stderr, "ERROR: %s\n", message);
            return;
        case LUT_LOG_WARNING:
            fprintf (stderr, "WARNING: %s\n", message);
            return;
        case LUT_LOG_INFO:
            fprintf (stderr, "INFO: %s\n", message);
            return;
        case LUT_LOG_DEBUG:
            fprintf (stderr, "DEBUG: %s\n", message);
            return;
    }
}

static lut_logging_funct logging_funct = default_logging_funct;
static pthread_mutex_t logging_lock = PTHREAD_MUTEX_INITIALIZER;

/** Get the current logging function
 *
 * @return The current logging function
 */
lut_logging_funct
lut_get_logging_funct (void)
{
    lut_logging_funct f;

    pthread_mutex_lock (&logging_lock);
    f = logging_funct;
    pthread_mutex_unlock (&logging_lock);
    return f;
}

/** Set a custom logging function
 *
 * @param f Logging function to call each time a log message is generated
 */
void
lut_set_logging_funct (lut_logging_funct f)
{
    pthread_mutex_lock (&logging_lock);
    logging_funct = f;
    pthread_mutex_unlock (&logging_lock);
}

/** Log a message
 *
 * @param s Severity of the message
 * @param fmt Format string for the message
 * @param ... Parameters for the format string
 */ 
void
lut_log (lut_log_severity s, const char *fmt, ...)
{
    va_list ap;

    va_start (ap, fmt);
    lut_logv (s, fmt, ap);
    va_end (ap);
}

/** Log a message
 * 
 * @param s Severity of the message
 * @param fmt Format string for the message
 * @param ap Argument list for the format string
 */
void
lut_logv (lut_log_severity s, const char *fmt, va_list ap)
{
    if (!logging_funct)
        return;
    else {
        char buffer[512];
        vsnprintf (buffer, 512, fmt, ap);
        logging_funct (s, buffer);
    }
}

/** Log an error message
 * 
 * @param fmt Format string for the message
 * @param ... Parameters for the format string
 */
void
lut_log_error (const char *fmt, ...)
{
    va_list ap;

    va_start (ap, fmt);
    lut_logv (LUT_LOG_ERROR, fmt, ap);
    va_end (ap);
}

/** Log a warning message
 * 
 * @param fmt Format string for the message
 * @param ... Parameters for the format string
 */
void
lut_log_warning (const char *fmt, ...)
{
    va_list ap;

    va_start (ap, fmt);
    lut_logv (LUT_LOG_WARNING, fmt, ap);
    va_end (ap);
}

/** Log an info message
 * 
 * @param fmt Format string for the message
 * @param ... Parameters for the format string
 */
void
lut_log_info (const char *fmt, ...)
{
    va_list ap;

    va_start (ap, fmt);
    lut_logv (LUT_LOG_INFO, fmt, ap);
    va_end (ap);
}

/** Log a debug message
 * 
 * @param fmt Format string for the message
 * @param ... Parameters for the format string
 */
void
lut_log_debug (const char *fmt, ...)
{
    va_list ap;

    va_start (ap, fmt);
    lut_logv (LUT_LOG_DEBUG, fmt, ap);
    va_end (ap);
}
