/* libundertow - undertow-closure.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>

#include "undertow-closure-private.h"

static void
process_closure__errno (lut_closure *closure)
{
    lut_closure__errno *c = (lut_closure__errno *) closure;
    c->closure__errno (c->error, c->user_data);
    free (c);
}

lut_closure *
lut_closure_new__errno (void            (*f)(lut_errno, void *),
                        lut_errno       error,
                        void *          user_data)
{
    lut_closure__errno *closure;

    closure = malloc (sizeof (lut_closure__errno));
    closure->base.process = process_closure__errno;
    closure->closure__errno = f;
    closure->error = error;
    closure->user_data = user_data;
    return (lut_closure *) closure;
}

static void
process_closure__pointer (lut_closure *closure)
{
    lut_closure__pointer *c = (lut_closure__pointer *) closure;
    c->closure__pointer (c->pointer, c->user_data);
    free (c);
}

lut_closure *
lut_closure_new__pointer (void          (*f)(void *, void *),
                          void *        pointer,
                          void *        user_data)
{
    lut_closure__pointer *closure;

    closure = malloc (sizeof (lut_closure__pointer));
    closure->base.process = process_closure__pointer;
    closure->closure__pointer = f;
    closure->pointer = pointer;
    closure->user_data = user_data;
    return (lut_closure *) closure;
}

static void
process_closure__sizet_sizet_float_float (lut_closure *closure)
{
    lut_closure__sizet_sizet_float_float *c = (lut_closure__sizet_sizet_float_float *) closure;
    c->closure__sizet_sizet_float_float (c->st1, c->st2, c->f1, c->f2, c->user_data);
    free (c);
}

lut_closure *
lut_closure_new__sizet_sizet_float_float (void      (*f)(size_t, size_t, float, float, void *),
                                          size_t    st1,
                                          size_t    st2,
                                          float     f1,
                                          float     f2,
                                          void *    user_data)
{
    lut_closure__sizet_sizet_float_float *closure;

    closure = malloc (sizeof (lut_closure__sizet_sizet_float_float));
    closure->base.process = process_closure__sizet_sizet_float_float;
    closure->closure__sizet_sizet_float_float = f;
    closure->st1 = st1;
    closure->st2 = st2;
    closure->f1 = f1;
    closure->f2 = f2;
    closure->user_data = user_data;
    return (lut_closure *) closure;
}
