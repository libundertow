/* libundertow - undertow-channel.h
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-channel.h */

#ifndef LIBUNDERTOW_CHANNEL_H
#define LIBUNDERTOW_CHANNEL_H

#include <time.h>

#include "undertow-main.h"
#include "undertow-episode.h"
#include "undertow-error.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Undertow channel
 */
typedef struct {
    char *url;              /**< The URL of the channel */
    time_t updated_on;      /**< Time when the channel was last updated */
    char *title;            /**< The channel title, encoded in UTF-8 */
    char *link;             /**< The channel link */
    char *description;      /**< The channel description, encoded in UTF-8 */
    char *category;         /**< Channel category keywords */
    char *copyright;        /**< Copyright notice for the channel */
    char *image;            /**< Channel image */
} lut_channel;

/** Function prototype for the callback passed to lut_db_foreach_channel() */
typedef void (*lut_foreach_channel_funct)(lut_channel *, void *);

lut_channel *lut_channel_copy (lut_channel *channel);
void lut_channel_free (lut_channel *channel);
lut_errno lut_channel_exists (const char *url);
lut_errno lut_get_channel (const char *url, lut_channel **channel);
lut_errno lut_add_channel (const char *         url,
                           void                 (*on_channel_updated)(lut_channel *, void *),
                           void                 (*on_episode_added)(lut_episode *, void *),
                           void                 (*on_episode_expired)(lut_episode *, void *),
                           void                 (*on_completion)(lut_errno, void *),
                           void *               data);
lut_errno lut_refresh_channel (const char *     url,
                               void             (*on_channel_updated)(lut_channel *, void *),
                               void             (*on_episode_added)(lut_episode *, void *),
                               void             (*on_episode_expired)(lut_episode *, void *),
                               void             (*on_completion)(lut_errno, void *),
                               void *           data);
lut_errno lut_delete_channel (const char *url);
lut_errno lut_foreach_channel (lut_foreach_channel_funct f, void *data);

#ifdef __cplusplus
}
#endif

#endif
