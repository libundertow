/* libundertow - undertow-option.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-option.c */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>

static char *database_path = NULL;
static char *download_directory = NULL;
static int refresh_period = 360;            /* 6 hours */
pthread_mutex_t option_lock = PTHREAD_MUTEX_INITIALIZER;

/** Set database path option
 *
 * @param path The path to the database
 *
 * Set the path where to find (and possibly create) the database.
 * If the path is a directory, then the database to be opened/created
 * is assumed to be 'undertow.db'.  Otherwise if the path is a file,
 * then the file name is used for the database.  Note that intermediate
 * directories are *not* created.
 */
void
lut_option_set_database_path (const char *path)
{
    assert (path != NULL);
    assert (path[0] == '/');

    pthread_mutex_lock (&option_lock);
    if (database_path)
        free (database_path);
    database_path = strdup (path);
    pthread_mutex_unlock (&option_lock);
}

/** Get the database path
 *
 * @return The path to the database
 */
char *
lut_option_get_database_path (void)
{
    char *path = NULL;

    pthread_mutex_lock (&option_lock);
    if (database_path)
        path = strdup (database_path);
    pthread_mutex_unlock (&option_lock);
    return path;
}

void
lut_option_set_download_directory (const char *path)
{
    assert (path != NULL);
    assert (path[0] == '/');

    pthread_mutex_lock (&option_lock);
    if (download_directory)
        free (download_directory);
    download_directory = strdup (path);
    pthread_mutex_unlock (&option_lock);
}

char *
lut_option_get_download_directory (void)
{
    char *path = NULL;

    pthread_mutex_lock (&option_lock);
    if (download_directory)
        path = strdup (download_directory);
    pthread_mutex_unlock (&option_lock);
    return path;
}

void
lut_option_set_default_refresh_period (int minutes)
{
    assert (minutes >= 0);

    pthread_mutex_lock (&option_lock);
    refresh_period = minutes;
    pthread_mutex_unlock (&option_lock);
}

int
lut_option_get_default_refresh_period (void)
{
    int period;

    pthread_mutex_lock (&option_lock);
    period = refresh_period;
    pthread_mutex_unlock (&option_lock);
    return period;
}
