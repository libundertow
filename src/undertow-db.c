/* libundertow - undertow-db.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-db.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <pthread.h>
#include <sqlite3.h>

#include "undertow-db-private.h"
#include "undertow-download.h"
#include "undertow-error.h"
#include "undertow-logging.h"
#include "undertow-option.h"

#define UNDERTOW_DB_VERSION     "1"

/*
 * @defgroup DatabaseOperations Database Operations
 * @{
 *
 */

static sqlite3 *db = NULL;
static pthread_mutex_t db_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_key_t transaction_key;

static void
free_transaction_key (int *is_inside_transaction)
{
    if (*is_inside_transaction != 0)
        lut_log_error ("freeing transaction_key while inside a transaction!");
    free (is_inside_transaction);
}

static int
check_version (void *unused, int ncolumns, char **values, char **column_names)
{
    if (values == NULL)
        lut_log_debug ("check_version: no database version information detected");
    else
        lut_log_debug ("check_version: detected database version %s", values[0]);
    return 0;
}

/** Initialize the database
 *
 * @internal
 * @return 0 on success, otherwise -1 on error.
 */
int
lut_db_init (void)
{
    int exists, retval;
    char *db_path, *error = NULL;

    assert (sqlite3_threadsafe ());

    db_path = lut_option_get_database_path ();
    exists = access (db_path, F_OK);
    retval = sqlite3_open (db_path, &db);
    if (retval != SQLITE_OK) {
        lut_log_error ("lut_db_init: failed to open database: %s", sqlite3_errmsg (db));
        return -1;
    }
    if (exists != 0) {
        /* create the channels table */
        retval = sqlite3_exec (db,
                               "CREATE TABLE version (id INTEGER);"
                               "INSERT INTO version VALUES (" UNDERTOW_DB_VERSION ");"
                               "CREATE TABLE channels ("
                               "    url VARCHAR PRIMARY KEY,"
                               "    updated_on INTEGER UNSIGNED,"
                               "    title VARCHAR,"
                               "    link VARCHAR,"
                               "    description VARCHAR,"
                               "    image VARCHAR,"
                               "    category VARCHAR,"
                               "    copyright VARCHAR);"
                               "CREATE TABLE episodes ("
                               "    channel VARCHAR NOT NULL,"
                               "    published_on INTEGER UNSIGNED,"
                               "    title VARCHAR,"
                               "    description VARCHAR,"
                               "    image VARCHAR,"
                               "    content_url VARCHAR NOT NULL,"
                               "    content_size INTEGER,"
                               "    content_type VARCHAR,"
                               "    file_path VARCHAR);"
                               "CREATE TABLE downloads ("
                               "    file_path VARCHAR PRIMARY KEY,"
                               "    channel VARCHAR,"
                               "    content_url VARCHAR,"
                               "    is_complete INTEGER"
                               ");",
                               NULL,
                               NULL,
                               &error);
        if (retval != SQLITE_OK) {
            lut_log_error ("lut_db_init: failed to initialize the database tables: %s", error);
            if (error != NULL)
                sqlite3_free (error);
            return -1;
        }
    }

    /* check the database version */
    retval = sqlite3_exec (db,
                           "SELECT id FROM version ORDER BY id DESC LIMIT 1",
                           check_version,
                           NULL,
                           &error);
    if (retval != SQLITE_OK) {
        lut_log_debug ("lut_db_init: sqlite3_exec failed: %s", error);
        lut_log_error ("lut_db_init: failed to determine the database version");
        if (error != NULL)
            sqlite3_free (error);
        return -1;
    }

    /* create the transaction key for lock checking */
    pthread_key_create (&transaction_key, (void *) free_transaction_key);
    return 0;
}


/** Close the database
 *
 * @internal
 * @return MIRAGE_OK on success, otherwise a #lut_errno corresponding
 * to the error.
 */
int
lut_db_cleanup (void)
{
    int retval = sqlite3_close (db);
    if (retval != SQLITE_OK) {
        lut_log_error ("lut_db_cleanup: failed to close database: %s", sqlite3_errmsg (db));
        return -1;
    }
    pthread_key_delete (transaction_key);
    return 0;
}

/** Start a database transaction
 *
 * @return MIRAGE_OK on success, otherwise a #lut_errno corresponding
 * to the error.
 */
sqlite3 *
lut_db_begin_transaction (void)
{
    int retval, *is_in_transaction;
    
    is_in_transaction = pthread_getspecific (transaction_key);
    if (is_in_transaction == NULL) {
        is_in_transaction = calloc (1, sizeof (int));
        pthread_setspecific (transaction_key, is_in_transaction);
    }
    if (*is_in_transaction != 0) {
        lut_log_error ("LOCKING ERROR: already inside a transaction!");
        abort ();
    }
    pthread_mutex_lock (&db_lock);
    *is_in_transaction = 1;

    retval = sqlite3_exec (db, "BEGIN EXCLUSIVE;", NULL, NULL, NULL);
    if (retval != SQLITE_OK) {
        pthread_mutex_unlock (&db_lock);
        lut_log_error ("failed to start transaction: %s", sqlite3_errmsg (db));
        abort ();
    }

    return db;
}

/** Commit the database transaction
 *
 * @return MIRAGE_OK on success, otherwise a #lut_errno corresponding
 * to the error.
 */
void
lut_db_commit_transaction (void)
{
    int retval, *is_in_transaction;

    is_in_transaction = pthread_getspecific (transaction_key);
    if (is_in_transaction == NULL) {
        lut_log_error ("LOCKING ERROR: transaction key is not initialized!");
        abort ();
    }
    if (*is_in_transaction == 0) {
        lut_log_error ("LOCKING ERROR: no transaction pending!");
        abort ();
    }

    retval = sqlite3_exec (db, "COMMIT;", NULL, NULL, NULL);
    if (retval != SQLITE_OK) {
        pthread_mutex_unlock (&db_lock);
        lut_log_error ("failed to commit transaction: %s", sqlite3_errmsg (db));
        abort ();
    }
    *is_in_transaction = 0;

    pthread_mutex_unlock (&db_lock);
}

/** Abort the database transaction
 *
 * @return MIRAGE_OK on success, otherwise a #lut_errno corresponding
 * to the error.
 */
void
lut_db_abort_transaction (void)
{
    int retval, *is_in_transaction;

    is_in_transaction = pthread_getspecific (transaction_key);
    if (is_in_transaction == NULL) {
        lut_log_error ("LOCKING ERROR: transaction key is not initialized!");
        abort ();
    }
    if (*is_in_transaction == 0) {
        lut_log_error ("LOCKING ERROR: no transaction pending!");
        abort ();
    }

    retval = sqlite3_exec (db, "ROLLBACK;", NULL, NULL, NULL);
    if (retval != SQLITE_OK) {
        lut_log_error ("failed to abort transaction: %s", sqlite3_errmsg (db));
        abort ();
    }
    *is_in_transaction = 0;
    pthread_mutex_unlock (&db_lock);
}

/** @} */
