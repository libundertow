/* libundertow - undertow-main.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-main.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <errno.h>
#include <assert.h>

#include <curl/curl.h>

#include <libxml/threads.h>
#include <libxml/parser.h>

#include "undertow-closure-private.h"
#include "undertow-channel.h"
#include "undertow-db-private.h"
#include "undertow-download.h"
#include "undertow-download-bt-private.h"
#include "undertow-episode.h"
#include "undertow-main-private.h"
#include "undertow-error.h"
#include "undertow-logging.h"

/** Deferred Item
 * @internal
 */
struct deferred_item {
    pthread_t id;                       /**< Thread id */
    lut_deferred_funct f;               /**< Function to execute */
    void *data;                         /**< Data to pass to f */
    struct timespec start_time;         /**< When to begin executing f */
    int is_finished;                    /**< Whether f has executed */
};

/** Closure item
 * @internal
 */
struct closure_item {
    lut_closure *closure;               /**< The closure to execute */
    struct closure_item *next;          /**< Next item in linked list */
};

/* static function declarations */
static void *deferred_dispatcher (void *arg);

/* static global variables */
static int is_initialized = 0, is_running = 0;

static pthread_t deferred_dispatcher_id = 0;
static struct deferred_item **pending_items = NULL;
static int n_pending_items = 0;
static struct deferred_item **running_items = NULL;
static int n_running_items = 0;
static pthread_mutex_t deferred_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t deferred_cond = PTHREAD_COND_INITIALIZER;

static struct closure_item *closures_head = NULL, *closures_tail = NULL;

static int is_looping = 0;
static pthread_mutex_t main_loop_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t main_loop_cond = PTHREAD_COND_INITIALIZER;

/** 
 * @defgroup ControllingTheDispatcher Controlling the Dispatcher
 * @{
 */

/** Start the library
 *
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 *
 * Performs the work necessary to start using the library, including starting up
 * the dispatcher and opening the database.  This function must be called before
 * any other libundertow function, except for those starting with lut_option_*.
 */
lut_errno
lut_main_start (void)
{
    int retval;
    CURLcode code;

    pthread_mutex_lock (&deferred_lock);
    assert (!is_running);
    assert (deferred_dispatcher_id == 0);
    is_running = 1;

    /* initialize libxml2 and libcurl */
    if (!is_initialized) {
        xmlInitThreads ();
        xmlInitParser ();
        code = curl_global_init (CURL_GLOBAL_ALL);
        if (code != 0) {
            lut_log_debug ("lut_main_start: curl_global_init_failed: %s",
                           curl_easy_strerror (code));
            lut_log_error ("failed to start libundertow");
            retval = LUT_ERROR_DISPATCHER_NOT_RUNNING;
            goto finished;
        }
        is_initialized = 1;
    }

    /* initialize the database */
    if (lut_db_init () < 0) {
        retval = LUT_ERROR_DB_FAILURE;
        lut_log_error ("failed to start libundertow");
        goto finished;
    }    

    /* initialize bittorrent */
    if (lut_bt_init () < 0) {
        retval = LUT_ERROR_DISPATCHER_NOT_RUNNING;
        lut_log_error ("failed to start libundertow");
        goto finished;
    }

    /* start the deferred dispatcher */
    retval = pthread_create (&deferred_dispatcher_id,
                             NULL,
                             deferred_dispatcher,
                             NULL);
    if (retval != 0) {
        lut_log_debug ("lut_main_start: failed to create dispatcher thread: %s",
                       strerror (retval));
        lut_log_error ("failed to start libundertow");
        retval = LUT_ERROR_DISPATCHER_NOT_RUNNING;
        goto finished;
    }
    lut_log_debug ("lut_main_start: dispatcher is running (thread #%u)",
                   (unsigned int) deferred_dispatcher_id);
    retval = LUT_OK;

finished:
    pthread_mutex_unlock (&deferred_lock);
    return retval;
}

/** Stop the library
 *
 * Stops the library, canceling any long-running operations, and closes the database.
 */
lut_errno
lut_main_stop (void)
{
    pthread_t id;

    pthread_mutex_lock (&deferred_lock);
    if (!is_running) {
        pthread_mutex_unlock (&deferred_lock);
        return LUT_ERROR_DISPATCHER_NOT_RUNNING;
    }
    id = deferred_dispatcher_id;
    deferred_dispatcher_id = 0;
    is_running = 0;
    pthread_cancel (id);
    lut_log_debug ("lut_main_stop: canceled dispatcher (thread #%u)", id);
    pthread_mutex_unlock (&deferred_lock);
    pthread_join (id, NULL);
    lut_log_debug ("lut_main_stop: joined dispatcher (thread #%u)", id);
    pthread_mutex_lock (&deferred_lock);
    lut_db_cleanup ();
    pthread_mutex_unlock (&deferred_lock);
    return LUT_OK;
}

/** Determines whether or not the dispatcher is running.
 *
 * @returns non-zero if the dispatcher is running, otherwise zero.
 */
int
lut_main_is_running (void)
{
    int retval;

    pthread_mutex_lock (&deferred_lock);
    if (is_running)
        retval = 1;
    else
        retval = 0;
    pthread_mutex_unlock (&deferred_lock);
    return retval;
}

/* 
 * sort items in the work queue by their start time, so that work queue
 * items which should be run earliest are placed at the beginning of the
 * array, and any NULL items are placed at the end of the array.
 */
static int
compare_pending_items (const void *v1, const void *v2)
{
    const struct deferred_item *i1 = * (struct deferred_item **)v1;
    const struct deferred_item *i2 = * (struct deferred_item **)v2;

    if (i1 == NULL && i2 == NULL)
        return 0;
    if (i1 == NULL)
        return 1;
    if (i2 == NULL)
        return -1;
    if (i1->start_time.tv_sec < i2->start_time.tv_sec)
        return -1;
    if (i1->start_time.tv_sec > i2->start_time.tv_sec)
        return 1;
    if (i1->start_time.tv_nsec < i2->start_time.tv_nsec)
        return -1;
    if (i1->start_time.tv_nsec > i2->start_time.tv_nsec)
        return 1;
    return 0;
}

/* 
 * sort items in the 'running' queue by their pointer address, so that
 * lut_main_cancel_deferred can easily find a running item using
 * bsearch(),  and any NULL items are placed at the end of the array.
 */
static int
compare_running_items (const void *v1, const void *v2)
{
    const struct deferred_item *i1 = * ((struct deferred_item **)v1);
    const struct deferred_item *i2 = * ((struct deferred_item **)v2);

    if (i1 == NULL && i2 == NULL)
        return 0;
    if (i1 == NULL)
        return 1;
    if (i2 == NULL)
        return -1;
    if (i1 < i2)
        return -1;
    if (i1 > i2)
        return 1;
    return 0;
}

/*
 *
 */
static void *
execute_deferred_item (void *arg)
{
    struct deferred_item *item = (struct deferred_item *) arg;
    item->f (item->data);
    pthread_mutex_lock (&deferred_lock);
    item->is_finished = 1;
    pthread_cond_signal (&deferred_cond);
    pthread_mutex_unlock (&deferred_lock);
    return NULL;
}

/*
 *
 */
static void
deferred_dispatcher_cleanup (void *arg)
{
    /* FIXME: do proper cleanup */
    lut_log_debug ("deferred_dispatcher_cleanup: cleaning up work queue");
    /* 
     * if we were cancelled while waiting inside of pthread_cond_wait or
     * pthread_cond_timedwait, then we are holding the mutex, so we need to
     * release it.  If we aren't holding the mutex, then calling pthread_mutex_unlock
     * doesn't do anything (i don't think...?)
     *
     * anyways, if this call isn't here, lut_main_stop deadlocks.
     */
    pthread_mutex_unlock (&deferred_lock);
}

/*
 *
 */
static void *
deferred_dispatcher (void *arg)
{
    pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype (PTHREAD_CANCEL_DEFERRED, NULL);
    pthread_cleanup_push (deferred_dispatcher_cleanup, NULL);

    pthread_mutex_lock (&deferred_lock);

    /* this block of code loops forever until canceled */
    while (1) {
        int i, n_finished, retval;
        struct deferred_item dummy, *dummy_ptr = &dummy;
        struct timeval now;

        /* if there are no items in the queue, wait to be signaled */
        if (n_pending_items > 0)
            retval = pthread_cond_timedwait (&deferred_cond,
                                             &deferred_lock,
                                             &(pending_items[0]->start_time));
        else
            retval = pthread_cond_wait (&deferred_cond, &deferred_lock);
        /* pthread_cond_timedwait was interrupted by a signal, so restart */
        /*
        if (retval == EINTR)
            continue;
        */
        assert (retval != EINTR);

        /* process all waiting closures */
        while (closures_head) {
            struct closure_item *next = closures_head->next;
            closures_head->closure->process (closures_head->closure);
            free (closures_head);
            closures_head = next;
        }

        /* join any threads which have finished */
        for (i = 0, n_finished = 0; i < n_running_items; i++) {
            struct deferred_item *current_item = running_items[i];

            if (current_item->is_finished) {
                pthread_join (current_item->id, NULL);
                lut_log_debug ("deferred_dispatcher: deferred item finished (joined thread #%u)",
                                  (unsigned int) current_item->id);
                free (current_item);
                running_items[i] = NULL;
                n_finished++;
            }
        }
        if (n_finished > 0) {
            qsort (running_items, n_running_items, sizeof (struct deferred_item *), compare_running_items);
            n_running_items -= n_finished;
        }

        /*
         * we know that the work queue is sorted so the work items which
         * need to be executed earliest are at the beginning of the array,
         * so iterate over the array executing each work item which has a
         * start_time equal or less than the current time.
         */
        dummy.f = NULL;
        dummy.data = NULL;
        gettimeofday (&now, NULL);
        dummy.start_time.tv_sec = now.tv_sec;
        dummy.start_time.tv_nsec = now.tv_usec * 1000;
        for (i = 0; i < n_pending_items; i++) {
            struct deferred_item *current_item = pending_items[i];

            /* check whether timeout has occurred for the item */
            retval = compare_pending_items (&dummy_ptr, &current_item);
            if (retval < 0)
                    break;
            pthread_create (&current_item->id,
                            NULL,
                            execute_deferred_item,
                            current_item);
            lut_log_debug ("deferred_dispatcher: executing deferred item (thread #%u)",
                                (unsigned int) current_item->id);
            pending_items[i] = NULL;
            /* move deferred_item to running queue */
            running_items = realloc (running_items, sizeof (struct deferred_item *) * (n_running_items + 1));
            running_items[n_running_items] = current_item;
            n_running_items++;
        }

        /* only re-sort the queues if they have changed */
        if (i > 0) {
            /* re-sort the pending queue so NULL items are placed at the end of the array */
            qsort (pending_items, n_pending_items, sizeof (struct deferred_item *), compare_pending_items);
            n_pending_items -= i;
            lut_log_debug ("deferred_dispatcher: %i items left in the deferred queue", n_pending_items);
            /* re-sort the running queue for faster lookups */
            qsort (running_items, n_running_items, sizeof (struct deferred_item *), compare_running_items);
        }
    }

    pthread_cleanup_pop (1);    /* shouldn't get here */
    return NULL;
}

/** Execute a function in a new thread after the specified timeout
 * 
 * @param timeout The length of time to wait (in milliseconds) before executing f
 * @param handle Handle to the deferred
 * @param f The function to execute
 * @param data A pointer to opaque data which is passed unmodified to f
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 */
lut_errno
lut_main_defer_with_timeout (int                        timeout,
                             lut_deferred_handle *      handle,
                             lut_deferred_funct         f,
                             void *                     data)
{
    struct deferred_item *item;
    struct timeval now;

    /* WORK QUEUE LOCKED */
    pthread_mutex_lock (&deferred_lock);

    item = malloc (sizeof (struct deferred_item));
    item->id = 0;
    item->f = f;
    item->data = data;
    gettimeofday (&now, NULL);
    item->start_time.tv_sec = now.tv_sec + timeout;
    item->start_time.tv_nsec = now.tv_usec * 1000;
    item->is_finished = 0;
    if (handle)
        *handle = (lut_deferred_handle) item;

    pending_items = realloc (pending_items, sizeof (struct deferred_item *) * (n_pending_items + 1));
    pending_items[n_pending_items] = item;
    n_pending_items++;
    qsort (pending_items, n_pending_items, sizeof (struct deferred_item *), compare_pending_items);
    lut_log_debug ("lut_main_defer_with_timeout: %i items now in the deferred queue",
                   n_pending_items);
    pthread_cond_signal (&deferred_cond);
    pthread_mutex_unlock (&deferred_lock);
    /* WORK QUEUE UNLOCKED */

    return LUT_OK;
}

/** Execute a function in a new thread
 *
 * @param f The function to execute
 * @param data A pointer to opaque data which is passed unmodified to f
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 */
lut_errno
lut_main_defer (lut_deferred_handle *       handle,
                lut_deferred_funct          f,
                void *                      data)
{
    return lut_main_defer_with_timeout (0, handle, f, data);
}

/** Cancel a deferred operation
 * 
 * @param handle Handle to the deferred operation, as returned by lut_main_defer()
 * or lut_main_defer_with_timeout()
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 */
lut_errno
lut_main_cancel_deferred (lut_deferred_handle handle)
{
    struct deferred_item **deferred_dptr, *deferred_item;

    assert (handle != NULL);

    pthread_mutex_lock (&deferred_lock);
    deferred_dptr = bsearch (&handle,
                             running_items,
                             n_running_items,
                             sizeof (struct deferred_item *),
                             compare_running_items);
    if (deferred_dptr == NULL) {
        lut_log_debug ("lut_main_cancel_deferred: no such deferred item");
        pthread_mutex_unlock (&deferred_lock);
        return LUT_ERROR_DEFERRED_NO_SUCH_ITEM;
    }
    deferred_item = *deferred_dptr;
    *deferred_dptr = NULL;
    qsort (running_items, n_running_items, sizeof (struct deferred_item *), compare_running_items);
    n_running_items--;
    pthread_mutex_unlock (&deferred_lock);
    if (pthread_cancel (deferred_item->id) == 0) {
        if (pthread_join (deferred_item->id, NULL) == 0)
            lut_log_debug ("lut_main_cancel_deferred: cancelled thread #%u",
                           (unsigned int) deferred_item->id);
        else {
            lut_log_debug ("lut_main_cancel_deferred: failed to join thread #%u",
                           (unsigned int) deferred_item->id);
        }
    }
    else {
        lut_log_debug ("lut_main_cancel_deferred: failed to cancel thread #%u",
                       (unsigned int) deferred_item->id);
    }
    free (deferred_item);
    /*
     * even though cancellation may fail, at this point we still return LUT_OK,
     * since from the library user's perspective there is no corrective action which
     * can be taken.
     */
    return LUT_OK;
}

/** Queue an event for processing 
 *
 * @internal
 * @param f Event function to execute
 * @param type The event type which occurred.
 * @param object Data associated with the event
 * @param data User data
 */
void
lut_main_process_closure (lut_closure *closure)
{
    struct closure_item *item;

    pthread_mutex_lock (&deferred_lock);
    item = malloc (sizeof (struct closure_item));
    item->closure = closure;
    item->next = NULL;
    if (closures_head == NULL)
        closures_head = item;
    else
        closures_tail->next = item;
    closures_tail = item;
    pthread_mutex_unlock (&deferred_lock);
    pthread_cond_signal (&deferred_cond);
}

/** Enter a simple main-loop
 *
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 *
 * For cases where a simple main-loop is needed.  Called lut_main_loop_run()
 * will block the calling thread until lut_main_loop_quit() has been called.
 */
lut_errno
lut_main_loop_run (void)
{
    pthread_mutex_lock (&main_loop_lock);
    if (is_looping) {
        pthread_mutex_unlock (&main_loop_lock);
        lut_log_error ("lut_main_loop_run: main loop is already running");
        return LUT_ERROR_MAINLOOP_IS_RUNNING;
    }
    is_looping = 1;
    pthread_cond_wait (&main_loop_cond, &main_loop_lock);
    is_looping = 0;
    pthread_mutex_unlock (&main_loop_lock);
    return LUT_OK;
}

/** Break from the simple main-loop
 *
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 */
lut_errno
lut_main_loop_quit (void)
{
    pthread_mutex_lock (&main_loop_lock);
    if (!is_looping) {
        pthread_mutex_unlock (&main_loop_lock);
        lut_log_error ("lut_main_loop_quit: main loop is not running");
        return LUT_ERROR_MAINLOOP_NOT_RUNNING;
    }
    pthread_cond_signal (&main_loop_cond);
    pthread_mutex_unlock (&main_loop_lock);
    return LUT_OK;
}

/** @} */
