/* libundertow - undertow-logging.h
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-logging.h */

#ifndef LIBUNDERTOW_LOGGING_H
#define LIBUNDERTOW_LOGGING_H

#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Mirage log severity */
typedef enum {
    LUT_LOG_ERROR,       /**< A serious error occured */
    LUT_LOG_WARNING,     /**< An problem occured which the user should be notified about */
    LUT_LOG_INFO,        /**< Information which may be useful for the user when troubleshooting */
    LUT_LOG_DEBUG        /**< Information which may be useful for the developer when troubleshooting */
} lut_log_severity;

/** Function prototype for the callback passed to lut_set_logging_funct() */
typedef void (*lut_logging_funct)(lut_log_severity, const char *);

lut_logging_funct lut_get_logging_funct (void);
void lut_set_logging_funct (lut_logging_funct f);

void lut_log (lut_log_severity s, const char *fmt, ...);
void lut_logv (lut_log_severity s, const char *fmt, va_list ap);
void lut_log_error (const char *fmt, ...);
void lut_log_warning (const char *fmt, ...);
void lut_log_info (const char *fmt, ...);
void lut_log_debug (const char *fmt, ...);

#ifdef __cplusplus
}
#endif

#endif
