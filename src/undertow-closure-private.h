/* libundertow - undertow-closure-private.h
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef LIBUNDERTOW_CLOSURE_PRIVATE_H
#define LIBUNDERTOW_CLOSURE_PRIVATE_H

#include "undertow-error.h"

#ifdef __cplusplus
extern "C" {
#endif

struct lut_closure {
    void (*process)(struct lut_closure *);
};
typedef struct lut_closure lut_closure;

/** The function prototype for a callback which processes a closure */
typedef void (*lut_closure_funct)(lut_closure *);

typedef void (*lut_closure_funct__errno)(lut_errno, void *);

typedef struct {
    lut_closure base;
    void (*closure__errno)(lut_errno, void *);
    lut_errno error;
    void *user_data;
} lut_closure__errno;

lut_closure *
lut_closure_new__errno (void            (*f)(lut_errno, void *),
                        lut_errno       error,
                        void *          user_data);

typedef void (*lut_closure_funct__pointer)(void *, void *);

typedef struct {
    lut_closure base;
    void (*closure__pointer)(void *, void *);
    void *pointer;
    void *user_data;
} lut_closure__pointer;

lut_closure *
lut_closure_new__pointer (void          (*f)(void *, void *),
                          void *        pointer,
                          void *        user_data);

typedef void (*lut_closure_funct__sizet_sizet_float_float)(size_t, size_t, float, float);

typedef struct {
    lut_closure base;
    void (*closure__sizet_sizet_float_float)(size_t, size_t, float, float, void *);
    size_t st1;
    size_t st2;
    float f1;
    float f2;
    void *user_data;
} lut_closure__sizet_sizet_float_float;

lut_closure *
lut_closure_new__sizet_sizet_float_float (void      (*f)(size_t, size_t, float, float, void *),
                                          size_t    st1,
                                          size_t    st2,
                                          float     f1,
                                          float     f2,
                                          void *    user_data);

#ifdef __cplusplus
}
#endif

#endif
