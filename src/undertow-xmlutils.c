/* libundertow - undertow-xmlutils.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

void
lut_xml_normalize_string (char *string)
{
    char *start, *end, *tmp;

    start = string;
    while (*start != '\0') {
        if (!isspace (*start))
            break;
        start++;
    }
    if (*start == '\0') {
        *string = '\0';
        return;
    }
    tmp = start + 1;
    end = NULL;
    while (*tmp != '\0') {
        if (isspace (*tmp) && end == NULL)
           end = tmp - 1;
        if (!isspace (*tmp) && end != NULL)
           end = NULL;
        tmp++;
    }
    if (end == NULL)
        end = tmp;
    memmove (string, start, end - start);
}
