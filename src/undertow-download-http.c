/* libundertow - undertow-download-http.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-download.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <assert.h>
#include <pthread.h>

#include "undertow-closure-private.h"
#include "undertow-db-private.h"
#include "undertow-download.h"
#include "undertow-download-private.h"
#include "undertow-episode.h"
#include "undertow-error.h"
#include "undertow-http-private.h"
#include "undertow-logging.h"
#include "undertow-main-private.h"

static void
do_process_header (time_t           last_modified,
                   size_t           content_length,
                   const char *     content_type,
                   void *           data)
{
    ;
}

static void
do_update_progress (size_t          content_length,
                    size_t          transferred,
                    float           fraction,
                    float           speed,
                    void *          data)
{
    lut_download_ctxt *ctxt = (lut_download_ctxt *) data;
    lut_closure *c;

    if (!ctxt->on_progress)
        return;
    c = lut_closure_new__sizet_sizet_float_float (ctxt->on_progress,
                                                  content_length,
                                                  transferred,
                                                  fraction,
                                                  speed,
                                                  ctxt->data);
   lut_main_process_closure (c);
}

/**
 *
 */
void
lut_download_http (lut_download_ctxt *ctxt)
{
    int retval;

    ctxt->error = lut_http_save (ctxt->content_url,
                                 ctxt->file_path,
                                 do_process_header,
                                 do_update_progress,
                                 ctxt);
    if (ctxt->error == LUT_OK) {
        sqlite3 *db;
        char *statement;

        pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, NULL);

        db = lut_db_begin_transaction ();

        statement = sqlite3_mprintf ("UPDATE downloads SET is_complete=1 WHERE file_path=%Q;",
                                     ctxt->file_path);
        retval = sqlite3_exec (db, statement, NULL, NULL, NULL);
        sqlite3_free (statement);
        if (retval != SQLITE_OK) {
                lut_log_error ("failed to update downloads table: %s", sqlite3_errmsg (db));
                lut_db_abort_transaction ();
                ctxt->error = LUT_ERROR_DB_FAILURE;
        }
        else
            lut_db_commit_transaction ();
    }
}
