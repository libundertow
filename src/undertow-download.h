/* libundertow - undertow-download.h
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-download.h */

#ifndef LIBUNDERTOW_DOWNLOAD_H
#define LIBUNDERTOW_DOWNLOAD_H

#include <sys/types.h>

#include "undertow-episode.h"
#include "undertow-main.h"
#include "undertow-error.h"

#ifdef __cplusplus
extern "C" {
#endif

/** download status */
typedef enum {
    LUT_DOWNLOAD_STATE_NOT_RUNNING,     /**< Initial download state */
    LUT_DOWNLOAD_STATE_RUNNING,         /**< Download is running */
    LUT_DOWNLOAD_STATE_FINISHED         /**< Download is complete */
} lut_download_state;

/** download object */
typedef struct {
    lut_download_state state;           /**< The current download state */
    char *channel;                      /**< The channel where the file comes from */
    char *content_url;                  /**< Location of the file to download */
    char *file_path;                    /**< Destination where the download is saved */
} lut_download;

lut_download *lut_download_copy (lut_download *download);
void lut_download_free (lut_download *download);
lut_errno lut_get_download (const char *file_path, lut_download **download);
lut_errno lut_start_download (lut_episode *       episode,
                              lut_download **     download,
                              void                (*on_progress)(size_t, size_t, float, float, void *),
                              void                (*on_completion)(lut_errno, void *),
                              void *              data);
lut_errno lut_resume_download (const char *       file_path,
                               lut_download **    download,
                               void               (*on_progress)(size_t, size_t, float, float, void *),
                               void               (*on_completion)(lut_errno, void *),
                               void *             data);
lut_errno lut_cancel_download (const char *file_path);
lut_errno lut_delete_download (const char *file_path);

#ifdef __cplusplus
}
#endif

#endif
