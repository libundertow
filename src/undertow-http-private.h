/* libundertow - undertow-http-private.h
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-http.h */

#ifndef LIBUNDERTOW_HTTP_PRIVATE_H
#define LIBUNDERTOW_HTTP_PRIVATE_H

#include "undertow-error.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Function prototype for a callback which is executed after HTTP headers have been received */
typedef void (*lut_http_header_funct)(time_t, size_t, const char *, void *);

/** Function prototype for a callback which is executed periodically to update progress */
typedef void (*lut_http_progress_funct)(size_t,size_t,float,float,void *);

lut_errno lut_http_get (const char *                url,
                        char **                     buffer,
                        size_t *                    size,
                        lut_http_header_funct       header_funct,
                        lut_http_progress_funct     progress_funct,
                        void *                      data);

lut_errno lut_http_save (const char *               url,
                         const char *               path,
                         lut_http_header_funct      header_funct,
                         lut_http_progress_funct    progress_funct,
                         void *                     data);

#ifdef __cplusplus
}
#endif

#endif
