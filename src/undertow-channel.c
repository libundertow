/* libundertow - undertow-channel.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-channel.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include <sqlite3.h>

#include "undertow-channel.h"
#include "undertow-db-private.h"
#include "undertow-episode.h"
#include "undertow-error.h"
#include "undertow-http-private.h"
#include "undertow-logging.h"
#include "undertow-main-private.h"
#include "undertow-feed.h"


/** Copy a #lut_channel instance
 *
 * @param channel The #lut_channel instance to copy
 * @return A new #lut_channel instance
 */
lut_channel *
lut_channel_copy (lut_channel *channel)
{
    lut_channel *copy;

    assert (channel != NULL);
    copy = malloc (sizeof (lut_channel));
    memset (copy, 0, sizeof (lut_channel));
    if (channel->url)
        copy->url = strdup (channel->url);
    copy->updated_on = channel->updated_on;
    if (channel->title)
        copy->title = strdup (channel->title);
    if (channel->link)
        copy->link = strdup (channel->link);
    if (channel->description)
        copy->description = strdup (channel->description);
    if (channel->image)
        copy->image = strdup (channel->image);
    if (channel->category)
        copy->category = strdup (channel->category);
    if (channel->copyright)
        copy->copyright = strdup (channel->copyright);
    return copy;
}

/** Free a dynamically allocated #lut_channel instance
 *
 * @param channel The #lut_channel instance to free
 */
void
lut_channel_free (lut_channel *channel)
{
    if (channel->url)
        free (channel->url);
    if (channel->title)
        free (channel->title);
    if (channel->link)
        free (channel->link);
    if (channel->description)
        free (channel->description);
    if (channel->image)
        free (channel->image);
    if (channel->category)
        free (channel->category);
    if (channel->copyright)
        free (channel->copyright);
    free (channel);
}

static int
lut_get_channel_unlocked (const char *              url,
                          lut_channel **            channel,
                          sqlite3 *                 db)
{
    int retval;
    sqlite3_stmt *statement = NULL;
    char *sql;
    const char *tail;

    assert (url != NULL);
    assert (channel != NULL);

    sql = sqlite3_mprintf ("SELECT url,updated_on,title,link,description,copyright"
                           " FROM channels"
                           " WHERE url=%Q;", url);
    retval = sqlite3_prepare (db, sql, strlen (sql), &statement, &tail);
    sqlite3_free (sql);
    if (retval != SQLITE_OK) {
        lut_log_error ("lut_get_channel: %s", sqlite3_errmsg (db));
        return LUT_ERROR_DB_FAILURE;
    }

    retval = sqlite3_step (statement);
    if (retval == SQLITE_ROW) {
        const unsigned char *url, *title, *link, *description, *image, *copyright;
        time_t updated_on;
        lut_channel *channel_ptr;

        url = sqlite3_column_text (statement, 0);
        updated_on = (time_t) sqlite3_column_int (statement, 1);
        title = sqlite3_column_text (statement, 2);
        link = sqlite3_column_text (statement, 3);
        description = sqlite3_column_text (statement, 4);
        image = sqlite3_column_text (statement, 5);
        copyright = sqlite3_column_text (statement, 6);

        channel_ptr = malloc (sizeof (lut_channel));
        memset (channel_ptr, 0, sizeof (lut_channel));
        if (url)
            channel_ptr->url = strdup ((char *)url);
        channel_ptr->updated_on = updated_on;
        if (title)
            channel_ptr->title = strdup ((char *)title);
        if (link)
            channel_ptr->link = strdup ((char *)link);
        if (description)
            channel_ptr->description = strdup ((char *)description);
        if (image)
            channel_ptr->image = strdup ((char *)image);
        if (copyright)
            channel_ptr->copyright = strdup ((char *)copyright);
        *channel = channel_ptr;
        retval = LUT_OK;
    }
    else {
        *channel = NULL;
        retval = LUT_ERROR_DB_NO_SUCH_ITEM;
    }
    sqlite3_finalize (statement);

    return retval;
}

/** Retrieve a channel from the database
 *
 * @param url The URL of the channel to retrieve
 * @param channel Upon success, channel is set to the location of a newly allocated #lut_channel
 * @return LUT_OK on success, otherwise a #lut_errno corresponding to the error.
 */
lut_errno
lut_get_channel (const char *url, lut_channel **channel)
{
    sqlite3 *db;
    int retval;

    db = lut_db_begin_transaction ();
    retval = lut_get_channel_unlocked (url, channel, db);
    if (retval != LUT_OK)
        lut_db_abort_transaction ();
    else
        lut_db_commit_transaction ();
    return retval;
}

static lut_errno
lut_channel_exists_unlocked (const char *url, sqlite3 *db)
{
    int retval, exists;
    sqlite3_stmt *statement;
    char *sql;
    const char *tail;

    assert (url != NULL);

    sql = sqlite3_mprintf ("SELECT url FROM channels WHERE url=%Q;", url);
    retval = sqlite3_prepare (db, sql, strlen (sql), &statement, &tail);
    sqlite3_free (sql);
    if (retval == SQLITE_OK) {
        if (sqlite3_step (statement) == SQLITE_ROW)
            exists = LUT_OK;
        else
            exists = LUT_ERROR_DB_NO_SUCH_ITEM;
    }
    else  {
        exists = 0;
        lut_log_error ("lut_db_get_channel: %s", sqlite3_errmsg (db));
    }
    sqlite3_finalize (statement);
    return exists;
}

/** Tests whether the channel exists in the database
 *
 * @param url The URL of channel to test the existence of
 * @return Non-zero if the channel exists, otherwise 0 if there is no such channel
 * @return LUT_OK if the channel exists, LUT_ERROR_DB_NO_SUCH_ITEM if the channel doesn't
 * exist, otherwise a #lut_errno corresponding to the error.
 */
lut_errno
lut_channel_exists (const char *url)
{
    sqlite3 *db;
    int retval;

    db = lut_db_begin_transaction ();
    retval = lut_channel_exists_unlocked (url, db);
    lut_db_commit_transaction ();
    return retval;
}


/** Channel refresh context
 * @internal
 */
struct channel_refresh_ctxt
{
    char *url;                                              /**< URL of channel */
    char *feed_type;                                        /**< */
    lut_errno error;                                        /**< Error code to pass to completion */
    void *data;                                             /**< Data to pass to functions */
    void (*on_channel_updated)(lut_channel *, void *);      /**< */
    void (*on_episode_added)(lut_episode *, void *);        /**< */
    void (*on_episode_expired)(lut_episode *, void *);      /**< */
    void (*on_completion)(lut_errno, void *);               /**< Completion function */
};

static void
parse_header (time_t                            last_modified,
              size_t                            content_length,
              const char *                      mime_type,
              struct channel_refresh_ctxt *     ctxt )
{
    if (mime_type)
        ctxt->feed_type = strdup (mime_type);
}

static void
do_channel_refresh (struct channel_refresh_ctxt *ctxt)
{
    sqlite3 *db;
    sqlite3_stmt *query;
    char *buf_data = NULL, *statement, **expired_episodes = NULL;
    const char *tail;
    size_t buf_size = 0, n_episodes = 0, n_expired_episodes = 0;
    int i, retval;
    lut_channel *channel = NULL;
    lut_episode **episodes = NULL;

    /* retrieve the rss channel feed */
    ctxt->error = lut_http_get (ctxt->url,
                                &buf_data, &buf_size,
                                (lut_http_header_funct) parse_header,
                                NULL,
                                ctxt);
    if (ctxt->error != LUT_OK)
        goto cleanup;
    ctxt->error = lut_feed_load (ctxt->feed_type,
                                 ctxt->url,
                                 buf_data, buf_size,
                                 &channel,
                                 &episodes, &n_episodes);
    if (ctxt->error != LUT_OK)
        goto cleanup;


    /* update the channel */
    db = lut_db_begin_transaction ();
    statement = sqlite3_mprintf ("UPDATE channels SET"
                                 " updated_on=%u,"
                                 " title=%Q,"
                                 " link=%Q,"
                                 " description=%Q,"
                                 " image=%Q,"
                                 " category=%Q,"
                                 " copyright=%Q"
                                 " WHERE url=%Q;",
                                 channel->updated_on,
                                 channel->title,
                                 channel->link,
                                 channel->description,
                                 channel->image,
                                 channel->category,
                                 channel->copyright,
                                 channel->url);
    retval = sqlite3_exec (db, statement, NULL, NULL, NULL);
    sqlite3_free (statement);
    if (retval != SQLITE_OK) {
        lut_log_error ("failed to update channel data in database: %s", sqlite3_errmsg (db));
        lut_db_abort_transaction ();
        goto cleanup;
    }

    /* calculate expired episodes */
    statement = sqlite3_mprintf ("SELECT content_url"
                                 " FROM episodes"
                                 " WHERE channel=%Q;",
                                 channel->url);
    retval = sqlite3_prepare (db, statement, strlen (statement), &query, &tail);
    sqlite3_free (statement);
    if (retval != SQLITE_OK) {
        lut_log_error ("failed to query database: %s", sqlite3_errmsg (db));
        lut_db_abort_transaction ();
        goto cleanup;
    }
    while (sqlite3_step (query) == SQLITE_ROW) {
        const char *content_url = (const char *) sqlite3_column_text (query, 0);
        int exists = 0;
        for (i = 0; i < n_episodes && exists == 0; i++) {
            if (!strcmp (content_url, episodes[i]->content_url))
                exists = 1;
        }
        if (!exists) {
            expired_episodes = realloc (expired_episodes, sizeof (char *) * (n_expired_episodes+1));
            expired_episodes[n_expired_episodes++] = strdup (content_url);
        }
    }
    sqlite3_finalize (query);
 
    /* calculate added episodes */
    for (i = 0; i < n_episodes; i++) {
        statement = sqlite3_mprintf ("SELECT published_on"
                                     " FROM episodes"
                                     " WHERE channel=%Q and content_url=%Q;",
                                     episodes[i]->channel,
                                     episodes[i]->content_url);
        retval = sqlite3_prepare (db, statement, strlen (statement), &query, &tail);
        sqlite3_free (statement);
        if (retval == SQLITE_OK && sqlite3_step (query) == SQLITE_ROW) {
            sqlite3_finalize (query);
            lut_episode_free (episodes[i]);
            episodes[i] = NULL;
        }
        else {
            sqlite3_finalize (query);
            statement = sqlite3_mprintf ("INSERT INTO episodes"
                                         " (channel,content_url,content_size,content_type,published_on,title,description)"
                                         " VALUES (%Q,%Q,%u,%Q,%u,%Q,%Q);",
                                         episodes[i]->channel,
                                         episodes[i]->content_url,
                                         episodes[i]->content_length,
                                         episodes[i]->content_type,
                                         episodes[i]->published_on,
                                         episodes[i]->title,
                                         episodes[i]->description);
            retval = sqlite3_exec (db, statement, NULL, NULL, NULL);
            sqlite3_free (statement);
            if (retval != SQLITE_OK) {
                lut_log_error ("failed to insert episode into database: %s",
                               sqlite3_errmsg (db));
                lut_db_abort_transaction ();
                goto cleanup;
            }
        }
    }
    lut_db_commit_transaction ();

    /* emit 'episode expired' event */
    for (i = 0; i < n_expired_episodes; i++) {
        if (ctxt->on_episode_expired) {
            lut_episode *expired;
            if (lut_get_episode (channel->url, expired_episodes[i], &expired) == LUT_OK) {
                lut_closure *c = 
                    lut_closure_new__pointer ((lut_closure_funct__pointer)ctxt->on_episode_expired,
                                              expired,
                                              ctxt->data);
                lut_main_process_closure (c);
            }
            else
                lut_log_debug ("do_channel_refresh: expired episode %s doesn't exist in database",
                               expired_episodes[i]);
        }
        free (expired_episodes[i]);
    }
    free (expired_episodes);
    expired_episodes = NULL;

    /* emit 'episode added' event */
    for (i = 0; i < n_episodes; i++) {
        if (episodes[i] != NULL ) {
            if (ctxt->on_episode_added) {
                lut_closure *c =
                    lut_closure_new__pointer ((lut_closure_funct__pointer)ctxt->on_episode_added,
                                              episodes[i],
                                              ctxt->data);
                lut_main_process_closure (c);
            }
            else
                lut_episode_free (episodes[i]);
        }
    }
    free (episodes);
    episodes = NULL;

    /* emit 'channel updated' event */
    if (ctxt->on_channel_updated) {
        lut_closure *c =
            lut_closure_new__pointer ((lut_closure_funct__pointer)ctxt->on_channel_updated,
                                      channel,
                                      ctxt->data);
        lut_main_process_closure (c);
    }
    else
        lut_channel_free (channel);

cleanup:
    /* emit completion event */
    if (ctxt->on_completion) {
        lut_closure *c =
            lut_closure_new__errno (ctxt->on_completion, ctxt->error, ctxt->data);
        lut_main_process_closure (c);
    }
    if (ctxt->url)
        free (ctxt->url);
    if (ctxt->feed_type)
        free (ctxt->feed_type);
    if (buf_data)
        free (buf_data);
    free (ctxt);
}

/** Refresh the channel
 *
 * @param url The URL of the channel to refresh
 * @param event_funct Executed when an event occurs
 * @param completion_funct Executed when the operation has finished
 * @param data A pointer to opaque data which is passed unmodified to f
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 */
lut_errno
lut_refresh_channel (const char *   url,
                     void           (*on_channel_updated)(lut_channel *, void *),
                     void           (*on_episode_added)(lut_episode *, void *),
                     void           (*on_episode_expired)(lut_episode *, void *),
                     void           (*on_completion)(lut_errno, void *),
                     void *         data)
{
    struct channel_refresh_ctxt *ctxt;
    lut_errno retval;

    assert (url != NULL);

    /* verify that the channel exists */
    retval = lut_channel_exists (url);
    if (retval != LUT_OK)
        return retval;

    /* create the context */
    ctxt = malloc (sizeof (struct channel_refresh_ctxt));
    ctxt->url = strdup (url);
    ctxt->on_channel_updated = on_channel_updated;
    ctxt->on_episode_added = on_episode_added;
    ctxt->on_episode_expired = on_episode_expired;
    ctxt->on_completion = on_completion;
    ctxt->data = data;

    /* do the work in a separate thread */
    return lut_main_defer (NULL, (lut_deferred_funct) do_channel_refresh, ctxt);
}

/** Add the channel
 *
 * @param url The URL of the channel to refresh
 * @param event_funct The function to execute when an event occurs
 * @param completion_funct The function to execute when operation has finished
 * @param data A pointer to opaque data which is passed unmodified to f
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 */
lut_errno
lut_add_channel (const char *   url,
                 void           (*on_channel_updated)(lut_channel *, void *),
                 void           (*on_episode_added)(lut_episode *, void *),
                 void           (*on_episode_expired)(lut_episode *, void *),
                 void           (*on_completion)(lut_errno, void *),
                 void *         data)
{
    sqlite3 *db;
    char *statement;
    int retval;

    /* make sure the channel doesn't exist yet */
    retval = lut_channel_exists (url);
    if (retval != LUT_ERROR_DB_NO_SUCH_ITEM) {
        if (retval == LUT_OK)
            return LUT_ERROR_DB_ITEM_EXISTS;
        else
            return retval;
    }

     /* update the channel */
    db = lut_db_begin_transaction ();
    statement = sqlite3_mprintf ("INSERT INTO channels (url,updated_on) VALUES (%Q,%u);",
                                 url,
                                 time (NULL));
    retval = sqlite3_exec (db, statement, NULL, NULL, NULL);
    sqlite3_free (statement);
    if (retval != SQLITE_OK) {
        lut_log_error ("failed to insert channel into database: %s", sqlite3_errmsg (db));
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_FAILURE;
    }
    lut_db_commit_transaction ();

    /* refresh the channel */
    return lut_refresh_channel (url,
                                on_channel_updated,
                                on_episode_added,
                                on_episode_expired,
                                on_completion,
                                data);
}

/** Delete a channel in the database
 *
 * @param url The channel to delete
 * @return LUT_OK on success, otherwise a #lut_errno corresponding
 * to the error.
 */
lut_errno
lut_delete_channel (const char *url)
{
    sqlite3 *db;
    char *statement;
    int retval;

    assert (url != NULL);

    db = lut_db_begin_transaction ();
    statement = sqlite3_mprintf ("DELETE FROM channels WHERE url=%Q;", url);
    retval = sqlite3_exec (db, statement, NULL, NULL, NULL);
    sqlite3_free (statement);
    if (retval != SQLITE_OK) {
        lut_log_error ("failed to delete channel from database: %s", sqlite3_errmsg (db));
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_FAILURE;
    }

    statement = sqlite3_mprintf ("DELETE FROM episodes WHERE channel=%Q;", url);
    retval = sqlite3_exec (db, statement, NULL, NULL, NULL);
    sqlite3_free (statement);
    if (retval != SQLITE_OK) {
        lut_log_error ("failed to delete episodes from database: %s", sqlite3_errmsg (db));
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_FAILURE;
    }

    lut_db_commit_transaction ();

    return LUT_OK;
}

/** Perform an operation on each channel in the database
 *
 * @param f The function to execute
 * @param data A pointer to data which is passed unmodified to f
 * @return LUT_OK on success, otherwise a #lut_errno corresponding
 * to the error.
 */
lut_errno
lut_foreach_channel (lut_foreach_channel_funct f, void *data)
{
    sqlite3 *db;
    sqlite3_stmt *statement = NULL;
    const char *tail, *sql = "SELECT url,updated_on,title,link,description,image,copyright FROM channels;";
    lut_channel **channels = NULL;
    int n_channels = 0, i, retval;

    assert (f != NULL);

    db = lut_db_begin_transaction ();
    retval = sqlite3_prepare (db, sql, strlen (sql), &statement, &tail);
    if (retval != SQLITE_OK) {
        lut_log_error ("lut_db_foreach_channel: %s", sqlite3_errmsg (db));
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_FAILURE;
    }

    retval = sqlite3_step (statement);
    while (retval == SQLITE_ROW) {
        const unsigned char *url, *title, *link, *description, *image, *copyright;
        time_t updated_on;
        lut_channel *channel;

        url = sqlite3_column_text (statement, 0);
        updated_on = (time_t) sqlite3_column_int (statement, 1);
        title = sqlite3_column_text (statement, 2);
        link = sqlite3_column_text (statement, 3);
        description = sqlite3_column_text (statement, 4);
        image = sqlite3_column_text (statement, 5);
        copyright = sqlite3_column_text (statement, 6);

        channel = malloc (sizeof (lut_channel));
        memset (channel, 0, sizeof (lut_channel));
        if (url)
            channel->url = strdup ((char *)url);
        channel->updated_on = updated_on;
        if (title)
            channel->title = strdup ((char *)title);
        if (link)
            channel->link = strdup ((char *)link);
        if (description)
            channel->description = strdup ((char *)description);
        if (image)
            channel->image = strdup ((char *) image);
        if (copyright)
            channel->copyright = strdup ((char *)copyright);

        channels = realloc (channels, sizeof (lut_channel *) * (n_channels + 1));
        channels[n_channels++] = channel;

        retval = sqlite3_step (statement);
    }
    sqlite3_finalize (statement);
    lut_db_commit_transaction ();
    for (i = 0; i < n_channels; i++)
        f (channels[i], data);
    free (channels);
    return LUT_OK;
}
