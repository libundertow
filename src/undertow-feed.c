/* libundertow - undertow-feed.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-feed.c */

#include <string.h>
#include <assert.h>
#include <libxml/parser.h>

#include "undertow-feed-rss-private.h"
#include "undertow-feed-atom-private.h"
#include "undertow-xmlutils.h"

static int
determine_mimetype (const char *            url,
                    xmlDocPtr               doc,
                    lut_channel **          channel,
                    lut_episode ***         episodes,
                    size_t *                n_episodes)
{
    xmlNodePtr node;

    if (doc->children == NULL)
        return LUT_ERROR_FEED_INVALID;
    node = doc->children;
    while (node) {
        if (LUT_XML_IS_ELEMENT (node, "feed"))
            return lut_feed_atom_parse (url, doc, channel, episodes, n_episodes);
        if (LUT_XML_IS_ELEMENT (node, "rss"))
            return lut_feed_rss_parse (url, doc, channel, episodes, n_episodes);
        node = node->next;
    }
    return LUT_ERROR_FEED_INVALID;
}

int
lut_feed_load (const char *                 mimetype,
               const char *                 url,
               const char *                 buffer,
               size_t                       buffer_size,
               lut_channel **               channel,
               lut_episode ***              episodes,
               size_t *                     n_episodes)
{
    xmlDocPtr doc;

    assert (url != NULL);
    assert (buffer != NULL);
    assert (channel != NULL);
    assert (episodes != NULL);
    assert (n_episodes != NULL);

    doc = xmlReadMemory (buffer, buffer_size, "noname.xml", NULL, 0);
    if (doc == NULL)
        return LUT_ERROR_FEED_INVALID;
    if (mimetype) {
        if (!strcmp (mimetype, "application/rss+xml"))
            return lut_feed_rss_parse (url, doc, channel, episodes, n_episodes);
        if (!strcmp (mimetype, "application/atom+xml"))
            return lut_feed_atom_parse (url, doc, channel, episodes, n_episodes);
        /* let other mimetypes fall thru to heuristic matching */
    }
    return determine_mimetype (url, doc, channel, episodes, n_episodes);
}
