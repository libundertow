#ifndef LIBUNDERTOW_MEMORY_H
#define LIBUNDERTOW_MEMORY_H

void *lut_malloc0 (size_t size);
void *lut_calloc (size_t nmemb, size_t size);
void *lut_realloc (void *ptr, size_t size);
void lut_free (void *ptr);

#endif

