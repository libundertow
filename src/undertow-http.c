/* libundertow - undertow-http.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-http.c */

#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>
#include <assert.h>

#include <curl/curl.h>

#include "undertow-http-private.h"
#include "undertow-logging.h"
#include "undertow-error.h"

/** HTTP context
 * @internal
 */
struct http_ctxt
{
    CURL *curl;                                     /**< curl handle */
    char *buffer;                                   /**< buffer */
    size_t buflen;                                  /**< buflen */
    FILE *fp;                                       /**< handle to the local file */
    size_t content_length;                          /**< length of the remote file */
    char *content_type;                             /**< content_type */
    time_t last_modified;                           /**< last_modified */
    size_t nwritten;                                /**< amount of data written so far */
    struct timeval sample_start;                    /**< sample_start */
    int sample_bytes;                               /**< sample_bytes */
    lut_http_header_funct header_funct;          /**< header_funct */
    lut_http_progress_funct progress_funct;      /**< progress_funct */
    void *data;                                     /**< data */
};

static size_t
parse_header (void *ptr, size_t size, size_t nmemb, void *data)
{
    struct http_ctxt *ctxt = (struct http_ctxt *) data;
    CURLcode retval;
    long rcode;
    char *header;

    retval = curl_easy_getinfo (ctxt->curl, CURLINFO_RESPONSE_CODE, &rcode);
    if (retval != CURLE_OK || rcode >= 400)
        return -1;

    header = calloc (nmemb + 1, size);
    strncpy (header, (char *) ptr, size * nmemb);
    if (!strncmp (header, "\r\n", 2) && ctxt->header_funct)
        ctxt->header_funct (ctxt->last_modified,
                            ctxt->content_length,
                            ctxt->content_type,
                            ctxt->data);
    else {
        char *name, *value, *save_ptr;
        name = strtok_r (header, ": ", &save_ptr);
        if (name) {
            if (!strcasecmp (name, "Content-Length")) {
                value = strtok_r (NULL, " \r\n", &save_ptr);
                if (value)
                    ctxt->content_length = atoi (value);
            }
            if (!strcasecmp (name, "Content-Type")) {
                value = strtok_r (NULL, " \r\n", &save_ptr);
                if (value)
                    ctxt->content_type = strdup (value);
            }
            if (!strcasecmp (name, "Last-Modified")) {
                value = strtok_r (NULL, "\r\n", &save_ptr);
                if (value)
                    ctxt->last_modified = curl_getdate (value, NULL);
            }
        }
    }
    free (header);
    return size * nmemb;
}

static void
update_progress (struct http_ctxt *ctxt, size_t nwritten)
{
    if (ctxt->sample_start.tv_sec == 0)
        gettimeofday (&(ctxt->sample_start), NULL);
    else {
        struct timeval now;
        int elapsed_in_ms;

        /* calculate the time elapsed between sample_start and now */
        gettimeofday (&now, NULL);
        elapsed_in_ms = (now.tv_sec - ctxt->sample_start.tv_sec) * 1000;
        if (elapsed_in_ms == 0)
            elapsed_in_ms = (now.tv_usec - ctxt->sample_start.tv_usec) / 1000;
        else
            elapsed_in_ms = (elapsed_in_ms+(now.tv_usec/1000))-((1000000-ctxt->sample_start.tv_usec)/1000);

        /* call our progress function roughly once each 500 milliseconds */
        if (elapsed_in_ms > 500) {
            float bytes_per_sec;
            float fraction;
       
            if (elapsed_in_ms > 0)
                bytes_per_sec = (((float)ctxt->sample_bytes)/((float)elapsed_in_ms)) * 1000.0;
            else
                bytes_per_sec = 0.0;
            if (ctxt->content_length > 0)
                fraction = (((float)ctxt->nwritten)/((float)ctxt->content_length));
            else
                fraction = 0.0;
            ctxt->progress_funct (ctxt->content_length,
                                  ctxt->nwritten,
                                  fraction,
                                  bytes_per_sec,
                                  ctxt->data);
            ctxt->sample_start.tv_sec = now.tv_sec;
            ctxt->sample_start.tv_usec = now.tv_usec;
            ctxt->sample_bytes = 0;
        }
    }
}

static size_t
write_to_buffer (void *src, size_t size, size_t nmemb, void *data)
{
    size_t nwritten = size * nmemb;
    struct http_ctxt *ctxt = (struct http_ctxt *) data;

    ctxt->buffer = (char *) realloc (ctxt->buffer, ctxt->buflen + nwritten + 1);
    memcpy(&(ctxt->buffer[ctxt->buflen]), src, nwritten);
    ctxt->buflen += nwritten;
    ctxt->buffer[ctxt->buflen] = '\0';   /* add trailing null */
    if (ctxt->progress_funct)
        update_progress (ctxt, nwritten);
    return nwritten;
}

static size_t
write_to_file (void *src, size_t size, size_t nmemb, void *data)
{
    struct http_ctxt *ctxt = (struct http_ctxt *) data;
    size_t nwritten;

    nwritten = fwrite (src, size, nmemb, ctxt->fp);
    ctxt->nwritten += nwritten * size;
    ctxt->sample_bytes += nwritten * size;
    if (ctxt->progress_funct)
        update_progress (ctxt, nwritten);
    return nwritten;
}

static int
run_curl (CURL *curl)
{
    CURLcode result;
    long response_code;
    int retval;

    result = curl_easy_perform (curl);
    if (result == CURLE_OK)
        return LUT_OK;

    curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &response_code);
    switch (result) {
        case CURLE_COULDNT_RESOLVE_HOST:
            lut_log_debug ("run_curl: failed to resolve hostname");
            retval = LUT_ERROR_NET_HOST_LOOKUP_FAILED;
            break;
        case CURLE_HTTP_RETURNED_ERROR:
            lut_log_debug ("run_curl: server responded with  HTTP error %i", response_code);
        case CURLE_WRITE_ERROR:
        case CURLE_ABORTED_BY_CALLBACK:
            switch (response_code) {
                case 401:
                    retval = LUT_ERROR_NET_UNAUTHORIZED;
                    break;
                case 403:
                    retval = LUT_ERROR_NET_FORBIDDEN;
                    break;
                case 404:
                    retval = LUT_ERROR_NET_NOT_FOUND;
                    break;
                default:
                    retval = LUT_ERROR_NET_FAILURE;
                    break;
            }
            break;
        case CURLE_COULDNT_CONNECT:
        case CURLE_READ_ERROR:
        case CURLE_URL_MALFORMAT:
        case CURLE_TOO_MANY_REDIRECTS:
        case CURLE_INTERFACE_FAILED:
        default:
            lut_log_debug ("run_curl: HTTP operation failed: %s", curl_easy_strerror (result));
            retval = LUT_ERROR_NET_FAILURE;
            break;
    }
    return retval;
}

/** Retrieve data over HTTP
 * 
 * @param url URL of the file to retrieve
 * @param data Upon successful completion, data will point to a newly allocated buffer containing the file data
 * @param size Upon successful completion, the size_t pointed to by size will contain the file data length
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 *
 */
lut_errno
lut_http_get (const char *                  url,
              char **                       buffer,
              size_t *                      size,
              lut_http_header_funct         header_funct,
              lut_http_progress_funct       progress_funct,
              void *                        data)
{
    struct http_ctxt ctxt;
    lut_errno retval = LUT_OK;

    assert (url != NULL);
    assert (buffer != NULL);
    assert (size != NULL);

    ctxt.curl = NULL;
    ctxt.buffer = NULL;
    ctxt.buflen = 0;
    ctxt.content_length = 0;
    ctxt.content_type = NULL;
    ctxt.last_modified = -1;
    ctxt.nwritten = 0;
    ctxt.sample_start.tv_sec = 0;
    ctxt.sample_start.tv_usec = 0;
    ctxt.sample_bytes = 0;
    ctxt.header_funct = header_funct;
    ctxt.progress_funct = progress_funct;
    ctxt.data = data;

    ctxt.curl = curl_easy_init ();
    if (ctxt.curl == NULL) {
        retval = LUT_ERROR_NET_FAILURE;
        goto cleanup;
    }
    curl_easy_setopt (ctxt.curl, CURLOPT_URL, url);
    curl_easy_setopt (ctxt.curl, CURLOPT_WRITEFUNCTION, write_to_buffer);
    curl_easy_setopt (ctxt.curl, CURLOPT_WRITEDATA, (void *) &ctxt);
    curl_easy_setopt (ctxt.curl, CURLOPT_HEADERFUNCTION, parse_header);
    curl_easy_setopt (ctxt.curl, CURLOPT_HEADERDATA, (void *) &ctxt);
    curl_easy_setopt (ctxt.curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_easy_setopt (ctxt.curl, CURLOPT_USERAGENT, "undertow/0.1");
    retval = run_curl (ctxt.curl);
    if (retval == LUT_OK) {
        *buffer = ctxt.buffer;
        *size = ctxt.buflen;
    }

cleanup:
    if (ctxt.content_type)
        free (ctxt.content_type);
    if (retval != LUT_OK && ctxt.buffer)
        free (ctxt.buffer);
    if (ctxt.curl)
        curl_easy_cleanup (ctxt.curl);
    return retval;
}

/** Retrieve a file over HTTP and save a it to disk 
 *
 * @param url URL of the file to retrieve
 * @param path Local file path where the file data will be stored
 * @param progress_funct Function to be executed periodically as the download progresses
 * @param data Pointer to opaque data which will be passed unmodified to progress_funct
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 */
lut_errno
lut_http_save (const char *                 url,
               const char *                 path,
               lut_http_header_funct        header_funct,
               lut_http_progress_funct      progress_funct,
               void *                       data)
{
    struct http_ctxt ctxt;
    lut_errno retval = LUT_OK;

    assert (url != NULL);
    assert (path != NULL);

    ctxt.curl = NULL;
    ctxt.fp = NULL;
    ctxt.content_length = 0;
    ctxt.content_type = NULL;
    ctxt.last_modified = -1;
    ctxt.nwritten = 0;
    ctxt.sample_start.tv_sec = 0;
    ctxt.sample_start.tv_usec = 0;
    ctxt.sample_bytes = 0;
    ctxt.header_funct = header_funct;
    ctxt.progress_funct = progress_funct;
    ctxt.data = data;

    ctxt.curl = curl_easy_init ();
    if (ctxt.curl == NULL) {
        retval = LUT_ERROR_NET_FAILURE;
        goto cleanup;
    }
    ctxt.fp = fopen (path, "w");
    if (ctxt.fp == NULL) {
        lut_log_warning ("failed to open %s: %s", path, strerror (errno));
        retval = LUT_ERROR_NET_FAILURE;
        goto cleanup;
    }

    curl_easy_setopt (ctxt.curl, CURLOPT_URL, url);
    curl_easy_setopt (ctxt.curl, CURLOPT_WRITEFUNCTION, write_to_file);
    curl_easy_setopt (ctxt.curl, CURLOPT_WRITEDATA, (void *) &ctxt);
    curl_easy_setopt (ctxt.curl, CURLOPT_HEADERFUNCTION, parse_header);
    curl_easy_setopt (ctxt.curl, CURLOPT_HEADERDATA, (void *) &ctxt);
    curl_easy_setopt (ctxt.curl, CURLOPT_NOPROGRESS, 1);
    curl_easy_setopt (ctxt.curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_easy_setopt (ctxt.curl, CURLOPT_USERAGENT, "undertow/0.1");
    retval = run_curl (ctxt.curl);

cleanup:
    if (ctxt.content_type)
        free (ctxt.content_type);
    if (ctxt.fp)
        fclose (ctxt.fp);
    if (ctxt.curl)
        curl_easy_cleanup (ctxt.curl);
    return retval;
}
