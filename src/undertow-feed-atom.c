/* libundertow - undertow-feed-atom.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-feed-atom.c */

#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include "undertow-channel.h"
#include "undertow-db-private.h"
#include "undertow-episode.h"
#include "undertow-error.h"
#include "undertow-xmlutils.h"

int
lut_feed_atom_parse (const char *               url,
                     xmlDocPtr                  doc,
                     lut_channel **             channel,
                     lut_episode ***            episodes,
                     size_t *                   n_episodes)
{
    xmlNodePtr node;
    lut_channel *channel_ptr = NULL;
    lut_episode **episodes_ptr = NULL;
    time_t now = time (NULL);
    int i;

    *channel = NULL;
    *episodes = NULL;
    *n_episodes = 0;

    /* find the feed element */
    if (doc->children == NULL)
        return LUT_ERROR_FEED_INVALID;
    node = doc->children;
    while (node) {
        if (node == NULL)
            goto failed;
        if (LUT_XML_IS_ELEMENT (node, "feed")) {
            channel_ptr = malloc (sizeof (lut_channel));
            memset (channel_ptr, 0, sizeof (lut_channel));
            channel_ptr->url = strdup (url);
            channel_ptr->updated_on = now;
            break;
        }
        node = node->next;
    }

    /* parse the channel attributes */
    node = node->children;
    while (node) {
        if (LUT_XML_IS_ELEMENT (node, "title")) {
            if (channel_ptr->title)
                xmlFree (channel_ptr->title);
            channel_ptr->title = (char *) xmlNodeGetContent (node);
            lut_xml_normalize_string (channel_ptr->title);
        }
        if (LUT_XML_IS_ELEMENT (node, "subtitle")) {
            if (channel_ptr->description)
                xmlFree (channel_ptr->description);
            channel_ptr->description = (char *) xmlNodeGetContent (node);
            lut_xml_normalize_string (channel_ptr->description);
        }
        if (LUT_XML_IS_ELEMENT (node, "entry")) {
            xmlNodePtr item_node = node->children;
            lut_episode *episode = malloc (sizeof (lut_episode));
            memset (episode, 0, sizeof (lut_episode));
            episode->channel = strdup (url);
            episode->published_on = now;

            /* parse the item attributes */
            while (item_node) {
                if (LUT_XML_IS_ELEMENT (item_node, "title")) {
                    if (episode->title)
                        xmlFree (episode->title);
                    episode->title = (char *) xmlNodeGetContent (item_node);
                    lut_xml_normalize_string (episode->title);
                }
                if (LUT_XML_IS_ELEMENT (item_node, "content")) {
                    if (episode->description)
                        xmlFree (episode->description);
                    episode->description = (char *) xmlNodeGetContent (item_node);
                    lut_xml_normalize_string (episode->description);
                }
                if (LUT_XML_IS_NS_ELEMENT (item_node, "media", "content") && 
                  episode->content_url == NULL) {
                    char *str;

                    if (episode->content_url)
                        xmlFree (episode->content_url);
                    episode->content_url = (char *) xmlGetProp (item_node, BAD_CAST "url");
                    if (episode->content_type)
                        xmlFree (episode->content_type);
                    episode->content_type = (char *) xmlGetProp (item_node, BAD_CAST "type");
                    str = (char *) xmlGetProp (item_node, BAD_CAST "fileSize");
                    if (str) {
                        long int file_size;
                        char *end_ptr;
                        file_size = strtol (str, &end_ptr, 10);
                        if (*end_ptr == '\0')
                            episode->content_length = (size_t) file_size;
                        xmlFree (BAD_CAST str);
                    }
                }
                if (LUT_XML_IS_NS_ELEMENT (item_node, "media", "group") &&
                  episode->content_url == NULL) {
                    xmlNodePtr group_node = item_node->children;

                    while (group_node) {
                        if (LUT_XML_IS_NS_ELEMENT (group_node, "media", "content") &&
                          episode->content_url == NULL) {
                            char *str;

                            if (episode->content_url)
                                xmlFree (episode->content_url);
                            episode->content_url = (char *) xmlGetProp (group_node, BAD_CAST "url");
                            if (episode->content_type)
                                xmlFree (episode->content_type);
                            episode->content_type = (char *) xmlGetProp (group_node, BAD_CAST "type");
                            str = (char *) xmlGetProp (group_node, BAD_CAST "fileSize");
                            if (str) {
                                long int file_size;
                                char *end_ptr;
                                file_size = strtol (str, &end_ptr, 10);
                                if (*end_ptr == '\0')
                                    episode->content_length = (size_t) file_size;
                                xmlFree (BAD_CAST str);
                            }
                        }
                        group_node = group_node->next;
                    }
                }
                 item_node = item_node->next;
            }

            episodes_ptr = realloc (episodes_ptr, sizeof (lut_episode *) * (*n_episodes + 2));
            episodes_ptr[*n_episodes] = episode;
            episodes_ptr[*n_episodes + 1] = NULL;
            *n_episodes = *n_episodes + 1;
        }
        node = node->next;
    }

    /* perform post-parsing sanity checks */
    if (channel_ptr->url == NULL ||
        channel_ptr->title == NULL ||
        channel_ptr->description == NULL)
        goto failed;
    for (i = 0; i < *n_episodes; i++) {
        if (episodes_ptr[i]->channel == NULL ||
            episodes_ptr[i]->content_url == NULL ||
            (episodes_ptr[i]->title == NULL && episodes_ptr[i]->description == NULL))
            goto failed;
    }

    /* Atom parsing succeeded */
    *channel = channel_ptr;
    *episodes = episodes_ptr;
    xmlFreeDoc (doc);
    return LUT_OK;

    /* Atom parsing failed */
failed:
    xmlFreeDoc (doc);
    if (channel_ptr)
        lut_channel_free (channel_ptr);
    if (episodes_ptr)
        lut_episode_free_list (episodes_ptr, *n_episodes);
    return LUT_ERROR_FEED_INVALID;
}

/** Parse Atom data from the supplied buffer
 *
 * @param url The URL from which the Atom data was retrieved
 * @param buffer A pointer to the buffer containing the Atom data
 * @param buffer_size The length of the buffer pointed to by data
 * @param channel
 * @param episodes
 * @param n_episodes
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 *
 */
int
lut_feed_atom_load (const char *            url,
                    const char *            buffer,
                    size_t                  buffer_size, 
                    lut_channel **          channel,
                    lut_episode ***         episodes,
                    size_t *                n_episodes)
{
    xmlDocPtr doc;

    assert (buffer != NULL);
    assert (channel != NULL);
    assert (episodes != NULL);
    assert (n_episodes != NULL);

    doc = xmlReadMemory (buffer, buffer_size, "noname.xml", NULL, 0);
    if (doc == NULL)
        return LUT_ERROR_FEED_INVALID;
    return lut_feed_atom_parse (url, doc, channel, episodes, n_episodes);
}
