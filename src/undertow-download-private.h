/* libundertow - undertow-download-private.h
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef LIBUNDERTOW_DOWNLOAD_PRIVATE_H
#define LIBUNDERTOW_DOWNLOAD_PRIVATE_H

#include <sys/types.h>

#include "undertow-download.h"
#include "undertow-error.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    char *content_url;                                          /**< The URL to the episode content */
    char *content_type;                                         /**< The content type of the content_url */
    char *file_path;                                            /**< Destination where the download is saved */
    lut_errno error;                                            /**< Error code to pass to completion_funct */
    void (*on_progress)(size_t, size_t, float, float, void *);  /**< Progress callback */
    void (*on_completion)(lut_errno, void *);                   /**< Completion callback */
    void *data;                                                 /**< Data to pass to callbacks */
} lut_download_ctxt;

#ifdef __cplusplus
}
#endif

#endif
