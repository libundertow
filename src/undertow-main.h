/* libundertow - undertow-main.h
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-main.h */

#ifndef LIBUNDERTOW_MAIN_H
#define LIBUNDERTOW_MAIN_H

#include "undertow-error.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup ControllingTheDispatcher
 * @{
 */

/**
 * An opaque value used to identify deferred instances.
 */
typedef int * lut_deferred_handle;

/** 
 * The function prototype for the callback passed to
 * lut_main_defer() and lut_main_defer_with_timeout()
 */
typedef void (*lut_deferred_funct)(void *);

lut_errno lut_main_start (void);
lut_errno lut_main_stop (void);
int lut_main_is_running (void);
lut_errno lut_main_defer_with_timeout (int                        timeout,
                                       lut_deferred_handle *      handle,
                                       lut_deferred_funct         f,
                                       void *                     arg);
lut_errno lut_main_defer (lut_deferred_handle *                   handle,
                          lut_deferred_funct                      f,
                          void *                                  arg);
lut_errno lut_main_cancel_deferred (lut_deferred_handle handle);
lut_errno lut_main_loop_run (void);
lut_errno lut_main_loop_quit (void);

/** @} */

#ifdef __cplusplus
}
#endif

#endif
