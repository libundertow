/* libundertow - undertow-error.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-error.c */

#include <stdio.h>

#include "undertow-error.h"

static const char *error_list[] =
{
    "No Error",
    "File Not Found",
    "Network Failure",
    "DNS Hostname Lookup Failed",
    "Remote Item Doesn't Exist",
    "Access Was Denied To Remote Item",
    "Remote Item Needs authorization",
    "Database Failure",
    "Item Exists In Database",
    "Item Does Not Exist In Database",
    "Feed Data Is Invalid",
    "Deferred Item Was Cancelled",
    "Deferred Item Is No Longer Running",
    "Dispatcher Is Already Running",
    "Dispatcher Is Not Running",
    "Main Loop Is Already Running",
    "Main Loop Is Not Running"
};

/**
 * @defgroup ErrorHandling Error Handling
 *
 * @{
 */

/** Return a human-readable string describing an error
 *
 * @param error
 * @return A string constant describing the error.
 *
 *
 */
const char *
lut_strerror (lut_errno error)
{
    if (error >= 0 && error < LUT_LAST_ERROR)
        return error_list[error];
    return NULL;
}

/** @} */
