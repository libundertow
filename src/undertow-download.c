/* libundertow - undertow-download.c
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-download.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <assert.h>
#include <pthread.h>

#include "undertow-closure-private.h"
#include "undertow-db-private.h"
#include "undertow-download-private.h"
#include "undertow-download-http-private.h"
#include "undertow-download-bt-private.h"
#include "undertow-episode.h"
#include "undertow-error.h"
#include "undertow-http-private.h"
#include "undertow-logging.h"
#include "undertow-main-private.h"
#include "undertow-option.h"

/** Download item
 * @internal
 */
struct download_item
{
    char *file_path;                    /**< Local path to the download */
    lut_deferred_handle handle;    /**< Deferred handle */
};

static struct download_item **download_items = NULL;
static int n_download_items = 0;

static int
compare_download_items (const void *v1, const void *v2)
{
    const struct download_item **dl1, **dl2;

    dl1 = (const struct download_item **) v1;
    dl2 = (const struct download_item **) v2;
    if (*dl1 == NULL)
        return 1;
    if (*dl2 == NULL)
        return -1;
    return strcmp ((*dl1)->file_path, (*dl2)->file_path);
}

/** Copy a #lut_download instance
 *
 * @param download The #lut_download instance to copy
 * @return A new #lut_download instance
 */
lut_download *
lut_download_copy (lut_download *download)
{
    lut_download *copy;

    assert (download != NULL);
    copy = malloc (sizeof (lut_download));
    memset (copy, 0, sizeof (lut_download));
    copy->state = download->state;
    copy->channel = strdup (download->channel);
    copy->content_url = strdup (download->content_url);
    copy->file_path = strdup (download->file_path);
    return copy;
}

/** Free a dynamically allocated #lut_download instance
 *
 * @param download The #lut_download instance to free
 */
void
lut_download_free (lut_download *download)
{
    if (download->channel)
        free (download->channel);
    if (download->content_url)
        free (download->content_url);
    if (download->file_path)
        free (download->file_path);
    free (download);
}

/** Retrieve #lut_download from the database
 *
 * @param file_path Path to the download
 * @param download
 * @param download Upon success, download is set to the location of a newly allocated #lut_download
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 */
lut_errno
lut_get_download (const char *file_path, lut_download **download)
{
    sqlite3 *db;
    sqlite3_stmt *query;
    char *statement;
    const char *tail;
    int retval;
    const unsigned char *channel;
    const unsigned char *content_url;
    int is_complete, state;

    assert (file_path != NULL);
    assert (download != NULL);

    db = lut_db_begin_transaction ();
    statement = sqlite3_mprintf ("SELECT channel,content_url,is_complete"
                                 " FROM downloads"
                                 " WHERE file_path=%Q;",
                                 file_path);
    retval = sqlite3_prepare (db, statement, strlen (statement), &query, &tail);
    sqlite3_free (statement);
    if (retval != SQLITE_OK) {
        lut_log_error ("failed to get download %s from database: %s", file_path, sqlite3_errmsg (db));
        sqlite3_finalize (query);
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_FAILURE;
    }
    retval = sqlite3_step (query);
    if (retval != SQLITE_ROW) {
        sqlite3_finalize (query);
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_NO_SUCH_ITEM;
    }

    channel = sqlite3_column_text (query, 0);
    content_url = sqlite3_column_text (query, 1);
    is_complete = sqlite3_column_int (query, 2);
    if (is_complete)
        state = LUT_DOWNLOAD_STATE_FINISHED;
    else {
        struct download_item *dl_ptr, dl_item;
        dl_item.file_path = (char *) file_path;
        dl_ptr = &dl_item;
        if (bsearch (&dl_ptr,
                     download_items,
                     n_download_items,
                     sizeof (struct download_item *),
                     compare_download_items))
            state = LUT_DOWNLOAD_STATE_RUNNING;
        else
            state = LUT_DOWNLOAD_STATE_NOT_RUNNING;
    }
    *download = malloc (sizeof (lut_download));
    memset (*download, 0, sizeof (lut_download));
    (*download)->state = state;
    (*download)->channel = strdup ((const char *) channel);
    (*download)->content_url = strdup ((const char *) content_url);
    (*download)->file_path = strdup (file_path);
    sqlite3_finalize (query);

    lut_db_commit_transaction ();

    return LUT_OK;
}

/** Clean up before exiting the thread
 *
 */
static void
cleanup_download (void *arg)
{
    lut_download_ctxt *ctxt = (lut_download_ctxt *) arg;
    struct download_item **dl_doubleptr, *dl_ptr, dl_item;

    lut_db_begin_transaction ();

    /* remove the download_item */
    dl_item.file_path = ctxt->file_path;
    dl_ptr = &dl_item;
    dl_doubleptr = bsearch (&dl_ptr,
                            download_items,
                            n_download_items,
                            sizeof (struct download_item *),
                            compare_download_items);
    assert (dl_doubleptr != NULL);
    dl_ptr = *dl_doubleptr;
    *dl_doubleptr = NULL;
    qsort (download_items, n_download_items, sizeof (struct download_item *), compare_download_items);
    download_items = realloc (download_items, sizeof (struct download_item *) * (--n_download_items));
    lut_log_debug ("do_download_episode_cleanup: there are now %i running downloads", n_download_items);

    lut_db_commit_transaction ();

    /* if the operation did not succeed, then delete the file */
    if (ctxt->error != LUT_OK && ctxt->error != LUT_ERROR_DEFERRED_WAS_CANCELLED) {
        lut_log_debug ("do_download_episode_cleanup: failed to download file to %s: %s",
                            dl_ptr->file_path, lut_strerror (ctxt->error));
        lut_delete_download (dl_ptr->file_path);
    }

    /* if there was a completion function specified, then queue it */
    if (ctxt->on_completion) {
        lut_closure *c =
            lut_closure_new__errno (ctxt->on_completion, ctxt->error, ctxt->data);
        lut_main_process_closure (c);
     }

    /* clean up */
    free (dl_ptr->file_path);
    free (dl_ptr);
    if (ctxt->content_url)
        free (ctxt->content_url);
    if (ctxt->file_path)
        free (ctxt->file_path);
    free (ctxt);
}

/** Determine the correct download method
 *
 * For now, a content_type of 'application/x-bittorrent' is the only time where
 * we don't download using HTTP.  In the future this could be expanded to implement
 * downloads using Pando (as seen only on blip.tv, so far) or any other method.
 */
static void
do_download_episode (lut_download_ctxt *ctxt)
{
    pthread_cleanup_push (cleanup_download, ctxt);
    if (ctxt->content_type) {
        lut_log_debug ("do_download_episode: content-type is %s", ctxt->content_type);
        if (!strcasecmp (ctxt->content_type, "application/x-bittorrent"))
            lut_download_bt (ctxt);
    }
    else
        lut_download_http (ctxt);
    pthread_cleanup_pop (1);
}

/** @} */

/** Download an episode
 *
 * @param episode The episode to download
 * @param download Upon success, download is set to the location of a newly allocated #lut_download
 * @param event_funct The function to execute when an event occurs
 * @param completion_funct The function to execute upon completion
 * @param data A pointer to opaque data which is passed unmodified to the callbacks
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 *
 */
lut_errno
lut_start_download (lut_episode *       episode,
                    lut_download **     download,
                    void                (*on_progress)(size_t, size_t, float, float, void *),
                    void                (*on_completion)(lut_errno, void *),
                    void *              data)
{
    lut_download_ctxt *ctxt;
    sqlite3 *db;
    char *download_dir, *file_name, *file_path, *statement;
    int file_path_len, retval;
    struct download_item *dl_item;
    lut_deferred_handle handle;

    assert (episode != NULL);
    assert (download != NULL);

    *download = NULL;

    /* check whether we should resume the download */
    if (episode->file_path)
        return lut_resume_download (episode->file_path,
                                    download,
                                    on_progress,
                                    on_completion,
                                    data);

    /* we should start a new download.  first determine the file path */
    download_dir = lut_option_get_download_directory ();
    file_name = strrchr (episode->content_url, '/');
    if (file_name == NULL) {
        free (download_dir);
        lut_log_error ("couldn't determine file name for download");
        return LUT_ERROR_DB_FAILURE;
    }
    file_name++;
    file_path_len = strlen (file_name) + strlen (download_dir) + 9;
    file_path = malloc (file_path_len);
    snprintf (file_path, file_path_len, "%s/%s.XXXXXX", download_dir, file_name);
    free (download_dir);

    /* create a new file in the download directory */
    if ((retval = mkstemp (file_path)) < 0) {
        free (file_path);
        lut_log_debug ("lut_start_download: mkstemp failed: %s", strerror (errno));
        lut_log_error ("failed to create local file for download");
        return LUT_ERROR_DB_FAILURE;
    }
    close (retval);
    lut_log_info ("downloading %s to %s", episode->content_url, file_path);

    /* insert the download into the database */
    db = lut_db_begin_transaction ();
    statement = sqlite3_mprintf ("INSERT INTO downloads"
                                 " (file_path,channel,content_url,is_complete)"
                                 " VALUES (%Q,%Q,%Q, 0);",
                                 file_path,
                                 episode->channel,
                                 episode->content_url);
    retval = sqlite3_exec (db, statement, NULL, NULL, NULL);
    sqlite3_free (statement);
    if (retval != SQLITE_OK) {
        lut_log_debug ("lut_start_download: failed to insert into downloads table: %s",
                       sqlite3_errmsg (db));
        free (file_path);
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_FAILURE;
    }

    /* update the episode with the file path of the download */
    statement = sqlite3_mprintf ("UPDATE episodes"
                                 " SET file_path=%Q"
                                 " WHERE channel=%Q AND content_url=%Q;",
                                 file_path,
                                 episode->channel,
                                 episode->content_url);
    retval = sqlite3_exec (db, statement, NULL, NULL, NULL);
    sqlite3_free (statement);
    if (retval != SQLITE_OK) {
        lut_log_debug ("lut_start_download: failed to update episodes table: %s",
                       sqlite3_errmsg (db));
        free (file_path);
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_FAILURE;
    }

    /* pass the download instance back to the caller */
    *download = malloc(sizeof (lut_download));
    memset (*download, 0, sizeof (lut_download));
    (*download)->state = LUT_DOWNLOAD_STATE_RUNNING;
    (*download)->channel = strdup (episode->channel);
    (*download)->content_url = strdup (episode->content_url);
    (*download)->file_path = strdup (file_path);

    /* create the context */
    ctxt = malloc (sizeof (lut_download_ctxt));
    memset (ctxt, 0, sizeof (lut_download_ctxt));
    ctxt->content_url = strdup (episode->content_url);
    ctxt->content_type = strdup (episode->content_type);
    ctxt->file_path = strdup (file_path);
    ctxt->on_progress = on_progress;
    ctxt->on_completion = on_completion;
    ctxt->data = data;
    ctxt->error = LUT_ERROR_DEFERRED_WAS_CANCELLED;

    /* start the download */
    retval = lut_main_defer (&handle,
                             (lut_deferred_funct) do_download_episode,
                             ctxt);
    if (retval != LUT_OK) {
        lut_db_abort_transaction ();
        return retval;
    }

    /* add to our list of currently running downloads */
    dl_item = malloc (sizeof (struct download_item));
    dl_item->file_path = file_path;
    dl_item->handle = handle;
    download_items = realloc (download_items, sizeof (struct download_item *) * (n_download_items + 1));
    download_items[n_download_items++] = dl_item;
    qsort (download_items, n_download_items, sizeof (struct download_item *), compare_download_items);
    lut_log_debug ("lut_start_download: there are now %i running downloads", n_download_items);

    lut_db_commit_transaction ();

    return LUT_OK;
}

/** Resume a partially-downloaded episode
 *
 * @param download The download to resume
 * @param event_funct The function to execute when an event occurs
 * @param completion_funct The function to execute upon completion
 * @param data A pointer to opaque data which is passed unmodified to callbacks 
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 */
lut_errno
lut_resume_download (const char *       file_path,
                     lut_download **    download,
                     void               (*on_progress)(size_t, size_t, float, float, void *),
                     void               (*on_completion)(lut_errno, void *),
                     void *             data)
{
    lut_download_ctxt *ctxt;
    sqlite3 *db;
    struct download_item  *dl_ptr, dl_item;
    sqlite3_stmt *query;
    char *statement;
    const char *tail;
    int retval;
    const unsigned char *channel;
    const unsigned char *content_url;
    lut_deferred_handle handle;

    assert (file_path != NULL);
    assert (download != NULL);

    *download = NULL;

    db = lut_db_begin_transaction ();

    /* check whether the download is already running */
    dl_item.file_path = (char *) file_path;
    dl_ptr = &dl_item;
    if (bsearch (&dl_ptr,
                 download_items,
                 n_download_items,
                 sizeof (struct download_item *),
                 compare_download_items) != NULL) {
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_ITEM_EXISTS;
    }

    /* get download info from database */
    statement = sqlite3_mprintf ("SELECT channel,content_url,is_complete"
                                 " FROM downloads"
                                 " WHERE file_path=%Q;",
                                 file_path);
    retval = sqlite3_prepare (db, statement, strlen (statement), &query, &tail);
    sqlite3_free (statement);
    if (retval != SQLITE_OK) {
        lut_log_debug ("lut_resume_download: failed to get download %s from database: %s",
                       file_path, sqlite3_errmsg (db));
        sqlite3_finalize (query);
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_FAILURE;
    }
    /* download is not in the database */
    retval = sqlite3_step (query);
    if (retval != SQLITE_ROW) {
        sqlite3_finalize (query);
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_NO_SUCH_ITEM;
    }
    /* download has already completed */
    if (sqlite3_column_int (query, 2) != 0) {
        sqlite3_finalize (query);
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_ITEM_EXISTS;
    }

    /* pass the download instance back to the caller */
    channel = sqlite3_column_text (query, 0);
    content_url = sqlite3_column_text (query, 1);
    *download = malloc (sizeof (lut_download));
    memset (*download, 0, sizeof (lut_download));
    (*download)->state = LUT_DOWNLOAD_STATE_RUNNING;
    (*download)->channel = strdup ((const char *) channel);
    (*download)->content_url = strdup ((const char *) content_url);
    (*download)->file_path = strdup (file_path);

    sqlite3_finalize (query);

     /* create the context */
    ctxt = malloc (sizeof (lut_download_ctxt));
    memset (ctxt, 0, sizeof (lut_download_ctxt));
    ctxt->content_url = strdup ((const char *) content_url);
    ctxt->file_path = strdup (file_path);
    ctxt->on_progress = on_progress;
    ctxt->on_completion = on_completion;
    ctxt->data = data;
    ctxt->error = LUT_ERROR_DEFERRED_WAS_CANCELLED;

    /* start the download */
    retval = lut_main_defer (&handle, (lut_deferred_funct) do_download_episode, ctxt);
    if (retval != LUT_OK) {
        lut_db_abort_transaction ();
        return retval;
    }

    /* add to our list of currently running downloads */
    dl_ptr = malloc (sizeof (struct download_item));
    dl_ptr->file_path = strdup (file_path);
    dl_ptr->handle = handle;
    download_items = realloc (download_items, sizeof (struct download_item *) * (n_download_items + 1));
    download_items[n_download_items++] = dl_ptr;
    qsort (download_items, n_download_items, sizeof (struct download_item *), compare_download_items);
    lut_log_debug ("lut_resume_download: there are now %i running downloads", n_download_items);

    lut_db_commit_transaction ();

    return LUT_OK;
}

/** Cancel a download in progress
 *
 * @param file_path The path of the download to cancel
 * @return Returns LUT_OK if the operation was successful, otherwise an
 *  integer corresponding to the specific error code.
 */
lut_errno
lut_cancel_download (const char *file_path)
{
    struct download_item **dl_doubleptr, *dl_ptr, dl_item;
    lut_deferred_handle handle;

    lut_db_begin_transaction ();

    dl_item.file_path = (char *) file_path;
    dl_ptr = &dl_item;
    dl_doubleptr = bsearch (&dl_ptr,
                            download_items,
                            n_download_items,
                            sizeof (struct download_item *),
                            compare_download_items);
    if (dl_doubleptr == NULL) {
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_NO_SUCH_ITEM;
    }
    dl_ptr = *dl_doubleptr;
    handle = dl_ptr->handle;

    lut_db_commit_transaction ();

    lut_main_cancel_deferred (handle);
    return LUT_OK;
}

/** Delete a download in the database
 *
 * @param file_path The download to delete
 * @return LUT_OK on success, otherwise a #lut_errno corresponding
 * to the error.
 */
lut_errno
lut_delete_download (const char *file_path)
{
    sqlite3 *db;
    char *statement;
    int retval;

    assert (file_path != NULL);

    db = lut_db_begin_transaction ();
    statement = sqlite3_mprintf ("DELETE FROM downloads"
                                 " WHERE file_path=%Q;", file_path);
    retval = sqlite3_exec (db, statement, NULL, NULL, NULL);
    sqlite3_free (statement);
    if (retval != SQLITE_OK) {
        lut_log_debug ("lut_delete_download: failed to delete %s from downloads table: %s",
                       file_path, sqlite3_errmsg (db));
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_FAILURE;
    }
    statement = sqlite3_mprintf ("UPDATE episodes"
                                 " SET file_path=NULL WHERE file_path=%Q;", file_path);
    retval = sqlite3_exec (db, statement, NULL, NULL, NULL);
    sqlite3_free (statement);
    if (retval != SQLITE_OK) {
        lut_log_debug ("lut_delete_download: failed to update episodes table: %s",
                       sqlite3_errmsg (db));
        lut_db_abort_transaction ();
        return LUT_ERROR_DB_FAILURE;
    }
    lut_db_commit_transaction ();

    retval = unlink (file_path);
    if (retval < 0) {
        lut_log_debug ("lut_delete_download: failed to unlink %s: %s",
                       file_path, strerror (errno));
        lut_log_warning ("download %s has not been removed from filesystem", file_path, strerror (errno));
    }
    return LUT_OK;
}
