/* libundertow - undertow-xmlutils.h
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef LIBUNDERTOW_XMLUTILS_H
#define LIBUNDERTOW_XMLUTILS_H

#ifdef __cplusplus
extern "C" {
#endif

#define LUT_XML_IS_ELEMENT(x_node,x_name)                   \
    (x_node->type == XML_ELEMENT_NODE &&                    \
     xmlStrEqual (x_node->name, BAD_CAST x_name))

#define LUT_XML_IS_NS_ELEMENT(x_node,x_ns,x_name)           \
    (x_node->type == XML_ELEMENT_NODE &&                    \
     xmlStrEqual (x_node->name, BAD_CAST x_name) &&         \
     x_node->ns &&                                          \
     xmlStrEqual (x_node->ns->prefix, BAD_CAST x_ns))

void lut_xml_normalize_string (char *string);

#ifdef __cplusplus
}
#endif

#endif

