/* libundertow - undertow-episode.h
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-episode.h */

#ifndef LIBUNDERTOW_EPISODE_H
#define LIBUNDERTOW_EPISODE_H

#include <unistd.h>
#include <time.h>

#include "undertow-error.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Undertow Episode
 */
typedef struct {
    char *channel;              /**< The URL of the channel to which the episode belongs */
    char *content_url;          /**< The URL of the episode's content */
    time_t published_on;        /**< Date which the episode was published */
    size_t content_length;      /**< Length of the content, in bytes */
    char *content_type;         /**< Content MIME-type */
    char *title;                /**< The episode title */
    char *description;          /**< A description of the episode */
    char *file_path;            /**< The path to the file, if it has been downloaded */
} lut_episode;

/** Function prototype for the callback passed to lut_db_foreach_episode() */
typedef void (*lut_foreach_episode_funct)(lut_episode *, void *);

lut_episode *lut_episode_copy (lut_episode *episode);
void lut_episode_free (lut_episode *episode);
void lut_episode_free_list (lut_episode *episodes[], size_t n_episodes);
lut_errno lut_get_episode (const char *channel, const char *content_url, lut_episode **episode);
lut_errno lut_episode_exists (const char *channel, const char *content_url);
lut_errno lut_foreach_episode (const char *channel, lut_foreach_episode_funct f, void *data);

#ifdef __cplusplus
}
#endif

#endif
