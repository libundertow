/* libundertow - undertow-feed-rss-private.h
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**\file undertow-feed-rss-private.h */

#ifndef LIBUNDERTOW_FEED_RSS_PRIVATE_H
#define LIBUNDERTOW_FEED_RSS_PRIVATE_H

#include <unistd.h>
#include <libxml/parser.h>

#include "undertow-feed-rss.h"
#include "undertow-channel.h"
#include "undertow-episode.h"

#ifdef __cplusplus
extern "C" {
#endif

int lut_feed_rss_parse (const char *                url,
                        xmlDocPtr                   doc,
                        lut_channel **              channel,
                        lut_episode ***             episodes,
                        size_t *                    n_episodes);

#ifdef __cplusplus
}
#endif

#endif
