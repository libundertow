/* libundertow - undertow-download-bt.cpp
 * Copyright (C) 2008 Michael Frank <msfrank@syntaxjockey.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"

#ifdef ENABLE_BITTORRENT

#include <iostream>
#include <fstream>
#include <iterator>
#include <exception>
#include <boost/format.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <libtorrent/entry.hpp>
#include <libtorrent/bencode.hpp>
#include <libtorrent/session.hpp>
#include <libtorrent/torrent.hpp>

#include "undertow-closure-private.h"
#include "undertow-db-private.h"
#include "undertow-download-private.h"
#include "undertow-error.h"
#include "undertow-http-private.h"
#include "undertow-logging.h"
#include "undertow-main-private.h"

/* in order to call this code from C, we need to declare it 'extern C'.
 * also important to note that the extern C block should not enclose
 * preprocessor includes, otherwise we get a ridiculously long string of
 * compiler errors.
 */
extern "C" {

static libtorrent::session *bt_session = NULL;

int
lut_bt_init (void)
{
    boost::filesystem::path::default_name_check(boost::filesystem::no_check);
    try
    {
        bt_session = new libtorrent::session ();
        bt_session->listen_on(std::make_pair(6881, 6889));
        bt_session->set_severity_level (libtorrent::alert::debug);
        lut_log_debug ("bt: finished initialization");
        return 0;
    }
    catch (std::exception& e)
    {
        lut_log_error ("bt: caught exception: %s", e.what());
        return -1;
    }
}

int
lut_bt_cleanup (void)
{
    if (bt_session)
        delete bt_session;
    bt_session = NULL;
    lut_log_debug ("bt: cleanup complete");
    return 0;
}

void
lut_download_bt (lut_download_ctxt *ctxt)
{
    char *buffer = NULL;
    size_t buflen = 0;
    lut_errno retval;
    int old_state = -1;

    /* grab the torrent info file */
    lut_log_debug ("bt: downloading torrent info from %s", ctxt->content_url);
    retval = lut_http_get (ctxt->content_url, &buffer, &buflen, NULL, NULL, NULL);
    if (retval != LUT_OK) {
        lut_log_info ("bt: failed to download torrent info from %s: %s",
                      ctxt->content_url,
                      lut_strerror (retval));
        ctxt->error = retval;
        return;
    }

    try {
        /* convert the data into an entry object by bdecode()ing it */
        libtorrent::entry e = libtorrent::bdecode(buffer, buffer + buflen);
        libtorrent::torrent_handle handle =
            bt_session->add_torrent(libtorrent::torrent_info(e), "./");
        libtorrent::torrent_info info = handle.get_torrent_info ();
        lut_log_debug ("bt: downloading %s", handle.name().c_str());

        /* loop until either we have completed downloading or there was an error */
        while (1) {
            lut_closure *c;

            /* check for any alerts */
            /*
            std::auto_ptr<libtorrent::alert> a = bt_sessionpop_alert ();
            if (a.get () != NULL)
                lut_log_debug ("bt: alert: %s", a->msg ());
            */
            /* get the torrent's current status */
            libtorrent::torrent_status status = handle.status();
            if (status.state == old_state && old_state != 4)
                continue;
            switch (status.state) {
                case 0:
                    lut_log_debug ("bt: queued for checking");
                    break;
                case 1:
                    lut_log_debug ("bt: checking files");
                    break;
                case 2:
                    lut_log_debug ("bt: connecting to tracker");
                    break;
                case 3:
                    lut_log_debug ("bt: downloading metadata");
                    break;
                case 4:
                    c = lut_closure_new__sizet_sizet_float_float (ctxt->on_progress,
                                                                  info.total_size (),
                                                                  status.total_done,
                                                                  0.0,
                                                                  status.download_rate,
                                                                  ctxt->data);
                    lut_main_process_closure (c);
                    break;
                case 5:
                    lut_log_debug ("bt: finished download");
                    break;
                case 6:
                    lut_log_debug ("bt: seeding");
                    break;
                case 7:
                    lut_log_debug ("bt: allocating space for download");
                    break;
                default:
                    break;
            }
            if (status.state == 5)
                break;
            old_state = status.state;

            /* we don't want to busy loop, so we sleep for a second */
            sleep (1);
        }
    }
    catch (std::exception& e)
    {
        lut_log_error ("bt: caught exception: %s", e.what());
    }

    /* if successful, update the database */
    if (ctxt->error == LUT_OK) {
        sqlite3 *db;
        char *statement;
        int sqlite_retval;

        pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, NULL);

        db = lut_db_begin_transaction ();

        statement = sqlite3_mprintf ("UPDATE downloads SET is_complete=1 WHERE file_path=%Q;",
                                     ctxt->file_path);
        sqlite_retval = sqlite3_exec (db, statement, NULL, NULL, NULL);
        sqlite3_free (statement);
        if (sqlite_retval != SQLITE_OK) {
                lut_log_error ("failed to update downloads table: %s", sqlite3_errmsg (db));
                lut_db_abort_transaction ();
                ctxt->error = LUT_ERROR_DB_FAILURE;
        }
        else
            lut_db_commit_transaction ();
    }
}

}   /* end of the extern "C" block */

/********************************************************************/
/* If bittorrent support is not enabled, we use these stubs instead */
/********************************************************************/
#else

extern "C" {

lut_errno
lut_bt_init (void) { return LUT_OK; }

lut_errno
lut_bt_cleanup (void) { return LUT_OK; }

void
lut_download_bt (lut_download_ctxt *ctxt)
{
    /* maybe create a new error type LUT_ERROR_NET_NOT_IMPLEMENTED? */
    ctxt->error = LUT_ERROR_NET_FAILURE;
}

}
#endif
