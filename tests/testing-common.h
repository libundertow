#ifndef TESTING_COMMON_H
#define TESTING_COMMON_H

#include <stdio.h>
#include <stdlib.h>

#define TEST_PASSED         0
#define TEST_FAILED         1
#define TEST_SKIPPED        2

#define TEST_DB_PATH                "data/undertowdb"
#define TEST_DOWNLOAD_DIRECTORY     "data/videos"

#define die(...)                            \
    {                                       \
        fprintf (stderr, "FAILURE: ");      \
        fprintf (stderr, __VA_ARGS__);      \
        fprintf (stderr, "\n");             \
        exit (TEST_FAILED);                 \
    } 

#endif
