#include <libundertow.h>

#include "testing-common.h"

int
main (void)
{
    lut_errno retval;

    retval = lut_main_stop();
    if (retval != LUT_ERROR_DISPATCHER_NOT_RUNNING)
        die ("lut_main_stop() returned '%s'", lut_strerror (retval));

    retval = lut_main_defer_with_timeout (0, NULL, NULL, NULL);
    if (retval != LUT_ERROR_DISPATCHER_NOT_RUNNING)
        die ("lut_main_defer_with_timeout() returned '%s'", lut_strerror (retval));

    retval = lut_main_defer (NULL, NULL, NULL);
    if (retval != LUT_ERROR_DISPATCHER_NOT_RUNNING)
        die ("lut_main_defer() returned '%s'", lut_strerror (retval));

    retval = lut_main_cancel_deferred (0);
    if (retval != LUT_ERROR_DISPATCHER_NOT_RUNNING)
        die ("lut_main_cancel_deferred() returned '%s'", lut_strerror (retval));

    return TEST_PASSED;
}
