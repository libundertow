#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <libundertow.h>

#include "testing-common.h"

int
main (int argc, char *argv[])
{
    pid_t child1, child2;
    int status1, status2;

    if (argv[1] != NULL) {
        lut_errno retval;
        /*
         *
         */
        if (!strcmp (argv[1], "-child1")) {
            printf ("executing child1\n");
            lut_option_set_database_path (TEST_DB_PATH);
            lut_option_set_download_directory (TEST_DOWNLOAD_DIRECTORY);
            retval = lut_main_start ();
            if (retval != LUT_OK)
                die ("lut_main_start () returned '%s'", lut_strerror (retval));
            sleep (3);
            retval = lut_main_stop ();
            if (retval != LUT_OK)
                die ("lut_main_stop () returned '%s'", lut_strerror (retval));
            return TEST_PASSED;
        }
        /*
         *
         */
        if (!strcmp (argv[1], "-child2")) {
            printf ("executing child2\n");
            lut_option_set_database_path (TEST_DB_PATH);
            lut_option_set_download_directory (TEST_DOWNLOAD_DIRECTORY);
            retval = lut_main_start ();
            if (retval == LUT_ERROR_DISPATCHER_IS_RUNNING)
                return TEST_PASSED;
            die ("lut_main_start () returned '%s'", lut_strerror (retval));
        }
        die ("shouldn't have reached this point");
    }

    child1 = fork ();
    if (child1 == 0)
        execl (argv[0], "-child1", NULL);
    printf ("parent executed child1\n");
    sleep (1);
    child2 = fork ();
    if (child2 == 0)
        execl (argv[0], "-child2", NULL);
    printf ("parent executed child2\n");

    if (waitpid (child1, &status1, 0) < 0 || WIFEXITED (status1))
        die ("waitpid() failed for child1");
    printf ("child1 exited\n");
    if (waitpid (child2, &status2, 0) < 0 || WIFEXITED (status2))
        die ("waitpid() failed for child2");
    printf ("child2 exited\n");

    if (WEXITSTATUS (status1) == TEST_PASSED)
        die ("child1 test failed");   
    if (WEXITSTATUS (status2) == TEST_PASSED)
        die ("child2 test failed");   
    printf ("tests passed\n");
    return TEST_PASSED;
}
