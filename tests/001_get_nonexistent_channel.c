#include <libundertow.h>

#include "testing-common.h"

int
main (void)
{
    lut_channel *channel;
    lut_errno retval;

    lut_option_set_database_path (TEST_DB_PATH);
    lut_option_set_download_directory (TEST_DOWNLOAD_DIRECTORY);

    retval = lut_main_start ();
    if (retval != LUT_OK)
        die ("lut_main_start () returned '%s'", lut_strerror (retval));

    retval = lut_get_channel ("http://this.url.does.not.exist", &channel);
    if (retval != LUT_ERROR_DB_NO_SUCH_ITEM)
        die ("lut_get_channel () returned '%s'", lut_strerror (retval));

    retval = lut_channel_exists ("http://this.url.does.not.exist");
    if (retval != LUT_ERROR_DB_NO_SUCH_ITEM)
        die ("lut_channel_exists () returned '%s'", lut_strerror (retval));

    retval = lut_main_stop ();
    if (retval != LUT_OK)
        die ("lut_main_stop () returned '%s'", lut_strerror (retval));

    return TEST_PASSED;
}
